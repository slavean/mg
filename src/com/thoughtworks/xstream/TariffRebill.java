package com.thoughtworks.xstream;

import com.ibm.icu.math.BigDecimal;

public class TariffRebill {

	private String AcctId;
	private BigDecimal Qty;
    private BigDecimal TaxFreePrice;
    private BigDecimal Tax;
    private BigDecimal TaxFreeCharge;
    private BigDecimal TaxSum;
    private BigDecimal Charge;
	
    
    public TariffRebill(String acctId, BigDecimal qty, BigDecimal taxFreePrice, BigDecimal tax,
    		BigDecimal taxFreeCharge, BigDecimal taxSum, BigDecimal charge) {
		super();
		AcctId = acctId;
		Qty = qty;
		TaxFreePrice = taxFreePrice;
		Tax = tax;
		TaxFreeCharge = taxFreeCharge;
		TaxSum = taxSum;
		Charge = charge;
	}

    public TariffRebill(TariffRebill other) {
		super();
		this.AcctId = other.AcctId;
		this.Qty = new BigDecimal(other.Qty.toString());
		this.TaxFreePrice = new BigDecimal(other.TaxFreePrice.toString());
		this.Tax = new BigDecimal(other.Tax.toString());
		this.TaxFreeCharge = new BigDecimal(other.TaxFreeCharge.toString());
		this.TaxSum = new BigDecimal(other.TaxSum.toString());
		this.Charge = new BigDecimal(other.Charge.toString());
	}
    
	public BigDecimal getQty() {
		return Qty;
	}


	public void setQty(BigDecimal qty) {
		Qty = qty;
	}


	public BigDecimal getTaxFreePrice() {
		return TaxFreePrice;
	}


	public void setTaxFreePrice(BigDecimal taxFreePrice) {
		TaxFreePrice = taxFreePrice;
	}


	public BigDecimal getTax() {
		return Tax;
	}


	public void setTax(BigDecimal tax) {
		Tax = tax;
	}


	public BigDecimal getTaxFreeCharge() {
		return TaxFreeCharge;
	}


	public void setTaxFreeCharge(BigDecimal taxFreeCharge) {
		TaxFreeCharge = taxFreeCharge;
	}


	public BigDecimal getTaxSum() {
		return TaxSum;
	}


	public void setTaxSum(BigDecimal taxSum) {
		TaxSum = taxSum;
	}


	public BigDecimal getCharge() {
		return Charge;
	}


	public void setCharge(BigDecimal charge) {
		Charge = charge;
	}


	public String getAcctId() {
		return AcctId;
	}


	public void setAcctId(String acctId) {
		AcctId = acctId;
	}
    
    
    
}
