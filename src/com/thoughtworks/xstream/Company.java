package com.thoughtworks.xstream;

import java.util.ArrayList;
import java.util.List;

import com.ibm.icu.math.BigDecimal;

public class Company {




	private String SerijaNakladnoj;
	private String NomerNakladnoj;
	private String DataNakladnoj;
	private String LSPredprijatija;
	private String KodPredprijatija;
	private String Naimenovanie;
	private Address address = null;
	private List<Address> adresDostavki = new ArrayList<Address>();
	private String Bank;
	private String BankSvift;
	private String NomerDogovora;
	private String DataDogovora;

	private String RaschSchet;
	private String FiskKod;
	private String NDSKod;
	private String NomerDoverennosti;
	private String DataDoverennosti;
	private String ImjaDelegata;
	
	private BigDecimal SummaBezNDS;
	private BigDecimal SummaNDS;
	private BigDecimal ObshhajaSumma;
	
	private List<ServicePoint> Potreblenie = new ArrayList<ServicePoint>();
	private List<Tariff> tariffs = new ArrayList<Tariff>();
	private List<Tariff> corrections = new ArrayList<Tariff>();	
	
	private Peni peni = null;
	
    public Company(String SerijaNakladnoj,String NomerNakladnoj, String DataNakladnoj,
	String LSPredprijatija, String KodPredprijatija, String Naimenovanie,
	String Bank, String BankSvift, String NomerDogovora,String DataDogovora,
	String RaschSchet, String FiskKod, String NDSKod, String NomerDoverennosti,
	String DataDoverennosti, String ImjaDelegata, BigDecimal SummaBezNDS,
	BigDecimal SummaNDS, BigDecimal ObshhajaSumma) 
	{
            this.SerijaNakladnoj = SerijaNakladnoj;
			this.NomerNakladnoj = NomerNakladnoj;
			this.DataNakladnoj = DataNakladnoj;
			this.LSPredprijatija = LSPredprijatija;
			this.KodPredprijatija = KodPredprijatija;
			this.Naimenovanie = Naimenovanie;		
			this.Bank = Bank;
			this.BankSvift = BankSvift;
			this.NomerDogovora = NomerDogovora;
			this.DataDogovora = DataDogovora;
			this.RaschSchet = RaschSchet;
			this.FiskKod = FiskKod;
			this.NDSKod = NDSKod;
			this.NomerDoverennosti = NomerDoverennosti;
			this.DataDoverennosti = DataDoverennosti;
			this.ImjaDelegata = ImjaDelegata;
			this.SummaBezNDS = SummaBezNDS;
			this.SummaNDS = SummaNDS;
			this.ObshhajaSumma = ObshhajaSumma;
			
    }
    
    public Company(Company C) 
    		{
    	this.SerijaNakladnoj =C.SerijaNakladnoj;
    	this.NomerNakladnoj = C.NomerNakladnoj; 
    	this.DataNakladnoj = C.DataNakladnoj; 
    	this.LSPredprijatija = C.LSPredprijatija;
    	this.KodPredprijatija = C.KodPredprijatija;
    	this.Naimenovanie = C.Naimenovanie; 		
    	this.Bank = C.Bank;
    	this.BankSvift = C.BankSvift;
    	this.NomerDogovora = C.NomerDogovora;
    	this.DataDogovora = C.DataDogovora;
    	this.RaschSchet = C.RaschSchet;
    	this.FiskKod = C.FiskKod;
    	this.NDSKod = C.NDSKod;
    	this.NomerDoverennosti = C.NomerDoverennosti;
    	this.DataDoverennosti = C.DataDoverennosti;
    	this.ImjaDelegata = C.ImjaDelegata;
    	this.SummaBezNDS = new BigDecimal(C.SummaBezNDS.toString());
    	this.SummaNDS = new BigDecimal(C.SummaNDS.toString());
    	this.ObshhajaSumma = new BigDecimal(C.ObshhajaSumma.toString());
    			
    	this.Potreblenie = ((List) ((ArrayList) C.Potreblenie).clone());
    	int i = 0;
    	for (i=0;i<this.Potreblenie.size();i++)
    	{
    		this.Potreblenie.set(i, new ServicePoint(this.Potreblenie.get(i))/*this.Potreblenie.get(i).clone()*/);
    	}

    	
    	this.address = new Address(C.getAddress());				
    	this.peni = new Peni(C.getPeni());		

    	this.tariffs = ((List) ((ArrayList) C.tariffs).clone());
       	for (i=0;i<this.tariffs.size();i++)
    	{
    		this.tariffs.set(i, new Tariff(this.tariffs.get(i))/*this.Potreblenie.get(i).clone()*/);
    	}
       	
    	this.corrections = ((List) ((ArrayList) C.corrections).clone());
       	for (i=0;i<this.corrections.size();i++)
    	{
    		this.corrections.set(i, new Tariff(this.corrections.get(i))/*this.Potreblenie.get(i).clone()*/);
    	}
       	
    	this.adresDostavki = ((List) ((ArrayList) C.adresDostavki).clone());
/*       	for (i=0;i<this.adresDostavki.size();i++)
    	{
    		this.adresDostavki.set(i, new Address(this.adresDostavki.get(i)));
    	}
*/       	
    				
    	    }
    

	public String getSerijaNakladnoj() {
		return SerijaNakladnoj;
	}

	public void setSerijaNakladnoj(String SerijaNakladnoj) {
		this.SerijaNakladnoj = SerijaNakladnoj;
	}

	public String getNomerNakladnoj() {
		return NomerNakladnoj;
	}

	public void setNomerNakladnoj(String NomerNakladnoj) {
		this.NomerNakladnoj = NomerNakladnoj;
	}

	public String getDataNakladnoj() {
		return DataNakladnoj;
	}

	public void setDataNakladnoj(String DataNakladnoj) {
		this.DataNakladnoj = DataNakladnoj;
	}

	public String getLSPredprijatija() {
		return LSPredprijatija;
	}

	public void setLSPredprijatija(String LSPredprijatija) {
		this.LSPredprijatija = LSPredprijatija;
	}

	public String getKodPredprijatija() {
		return KodPredprijatija;
	}

	public void setKodPredprijatija(String KodPredprijatija) {
		this.KodPredprijatija = KodPredprijatija;
	}

	public String getNaimenovanie() {
		return Naimenovanie;
	}

	public void setNaimenovanie(String Naimenovanie) {
		this.Naimenovanie = Naimenovanie;
	}

	public String getBank() {
		return Bank;
	}

	public void setBank(String Bank) {
		this.Bank = Bank;
	}

	public String getBankSvift() {
		return BankSvift;
	}

	public void setBankSvift(String BankSvift) {
		this.BankSvift = BankSvift;
	}

	public String getNomerDogovora() {
		return NomerDogovora;
	}

	public void setNomerDogovora(String NomerDogovora) {
		this.NomerDogovora = NomerDogovora;
	}
	
	public String getDataDogovora() {
		return DataDogovora;
	}

	public void setDataDogovora(String dataDogovora) {
		DataDogovora = dataDogovora;
	}	

	public String getRaschSchet() {
		return RaschSchet;
	}

	public void setRaschSchet(String RaschSchet) {
		this.RaschSchet = RaschSchet;
	}

	public String getFiskKod() {
		return FiskKod;
	}

	public void setFiskKod(String FiskKod) {
		this.FiskKod = FiskKod;
	}

	public String getNDSKod() {
		return NDSKod;
	}

	public void setNDSKod(String NDSKod) {
		this.NDSKod = NDSKod;
	}

	public String getNomerDoverennosti() {
		return NomerDoverennosti;
	}

	public void setNomerDoverennosti(String NomerDoverennosti) {
		this.NomerDoverennosti = NomerDoverennosti;
	}

	public String getDataDoverennosti() {
		return DataDoverennosti;
	}

	public void setDataDoverennosti(String DataDoverennosti) {
		this.DataDoverennosti = DataDoverennosti;
	}

	public String getImjaDelegata() {
		return ImjaDelegata;
	}

	public void setImjaDelegata(String ImjaDelegata) {
		this.ImjaDelegata = ImjaDelegata;
	}

	public BigDecimal getSummaBezNDS() {
		return SummaBezNDS;
	}

	public void setSummaBezNDS(BigDecimal SummaBezNDS) {
		this.SummaBezNDS = SummaBezNDS;
	}

	public BigDecimal getSummaNDS() {
		return SummaNDS;
	}

	public void setSummaNDS(BigDecimal SummaNDS) {
		this.SummaNDS = SummaNDS;
	}

	public BigDecimal getObshhajaSumma() {
		return ObshhajaSumma;
	}

	public void setObshhajaSumma(BigDecimal ObshhajaSumma) {
		this.ObshhajaSumma = ObshhajaSumma;
	}

	
	
    public void addTariff(Tariff entry) {
    	tariffs.add(entry);
    }

    public List<Tariff> getTariffs() {
            return tariffs;
    }    
    
    public void addPotreblenie(ServicePoint entry) {
    	Potreblenie.add(entry);
    }

    public List<ServicePoint> getPotreblenie() {
            return Potreblenie;
    }
    
    public void addCorrections(Tariff entry) {
    	corrections.add(entry);
    }

    public List<Tariff> getCorrections() {
            return corrections;
    }

	public Peni getPeni() {
		return peni;
	}

	public void setPeni(Peni peni) {
		this.peni = peni;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}    


    public void setAdresDostavki(Address entry) {
    	adresDostavki.add(entry);
    }

    public List<Address> getAdresDostavki() {
            return adresDostavki;
    }
    
    
}  
