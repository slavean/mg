package com.thoughtworks.xstream;

import com.ibm.icu.math.BigDecimal;

public class ServicePoint implements Cloneable{

	    @Override
	public ServicePoint clone() throws CloneNotSupportedException {
	    	ServicePoint clone = new ServicePoint(this);
	    	
	    	return clone;
		
	}
		private String TochkaUcheta;
		private String AdresTU;
		private String NomerPU;
		private String NachPokaz;
		private String KonPokaz;
		private BigDecimal Obiem;
		private String EdIzm;
		
		public ServicePoint(String TochkaUcheta, String AdresTU, String NomerPU, String NachPokaz, String KonPokaz, BigDecimal Obiem, String EdIzm) {

		    this.TochkaUcheta=TochkaUcheta;
		    this.AdresTU=AdresTU;
		    this.NomerPU=NomerPU;
		    this.NachPokaz=NachPokaz;
		    this.KonPokaz=KonPokaz;
		    this.Obiem=Obiem;
		    this.EdIzm=EdIzm;
			
		}
		
		public ServicePoint(ServicePoint other) {

		    this.TochkaUcheta=other.TochkaUcheta;
		    this.AdresTU=other.AdresTU;
		    this.NomerPU=other.NomerPU;
		    this.NachPokaz=other.NachPokaz;
		    this.KonPokaz=other.KonPokaz;
		    this.Obiem=new BigDecimal(other.Obiem.toString());
		    this.EdIzm=other.EdIzm;
			
		}		
		
		public String getTochkaUcheta() {
			return TochkaUcheta;
		}
		public void setTochkaUcheta(String tochkaUcheta) {
			TochkaUcheta = tochkaUcheta;
		}
		public String getAdresTU() {
			return AdresTU;
		}
		public void setAdresTU(String adresTU) {
			AdresTU = adresTU;
		}
		public String getNomerPU() {
			return NomerPU;
		}
		public void setNomerPU(String nomerPU) {
			NomerPU = nomerPU;
		}
		public String getNachPokaz() {
			return NachPokaz;
		}
		public void setNachPokaz(String nachPokaz) {
			NachPokaz = nachPokaz;
		}
		public String getKonPokaz() {
			return KonPokaz;
		}
		public void setKonPokaz(String konPokaz) {
			KonPokaz = konPokaz;
		}
		public BigDecimal getObiem() {
			return Obiem;
		}
		public void setObiem(BigDecimal obiem) {
			Obiem = obiem;
		}
		public String getEdIzm() {
			return EdIzm;
		}
		public void setEdIzm(String edIzm) {
			EdIzm = edIzm;
		}

}
