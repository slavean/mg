package com.thoughtworks.xstream;

import java.util.ArrayList;
import java.util.List;
import com.ibm.icu.math.BigDecimal;

public class Tariffs {
    private List<Tariff> tarrifs = new ArrayList<Tariff>();

    public Tariffs() {
    }

    public void addTariff(Tariff entry) {
    	tarrifs.add(entry);
    }

    public List<Tariff> getTariffs() {
            return tarrifs;
    }

    public Tariff searchTariff(String acctId, BigDecimal Tax, BigDecimal TaxFreePrice) {
        for (Tariff t : tarrifs)
        {
        	if ((t.getProcentNDS().equals(Tax)) && (t.getCenaBezNDS().equals(TaxFreePrice)) && (t.getAcctId_T().equals(acctId)))
        	{	
        		
        		return t;
        	}
       
        }
        return null;
}
}


