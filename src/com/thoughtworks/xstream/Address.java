package com.thoughtworks.xstream;

public class Address {

    private String PochtOtdel;
	private String Adm;
	private String Gorod;
	private String Ulica;
	private String Dom;
	private String Kvartira;
	
	public Address(String pochtOtdel, String adm, String gorod, String ulica,
			String dom, String kvartira) {
		super();
		PochtOtdel = pochtOtdel;
		Adm = adm;
		Gorod = gorod;
		Ulica = ulica;
		Dom = dom;
		Kvartira = kvartira;
	}
	
	public Address(Address other) {
		super();
		this.PochtOtdel = other.PochtOtdel;
		this.Adm = other.Adm;
		this.Gorod = other.Gorod;
		this.Ulica = other.Ulica;
		this.Dom = other.Dom;
		this.Kvartira = other.Kvartira;
	}

	public String getPochtOtdel() {
		return PochtOtdel;
	}

	public void setPochtOtdel(String pochtOtdel) {
		PochtOtdel = pochtOtdel;
	}

	public String getAdm() {
		return Adm;
	}

	public void setAdm(String adm) {
		Adm = adm;
	}

	public String getGorod() {
		return Gorod;
	}

	public void setGorod(String gorod) {
		Gorod = gorod;
	}

	public String getUlica() {
		return Ulica;
	}

	public void setUlica(String ulica) {
		Ulica = ulica;
	}

	public String getDom() {
		return Dom;
	}

	public void setDom(String dom) {
		Dom = dom;
	}

	public String getKvartira() {
		return Kvartira;
	}

	public void setKvartira(String kvartira) {
		Kvartira = kvartira;
	}
		

	
}
