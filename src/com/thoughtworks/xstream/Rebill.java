package com.thoughtworks.xstream;

import java.util.ArrayList;
import java.util.List;

public class Rebill {
    private List<TariffRebill> tarrifs = new ArrayList<TariffRebill>();

    public Rebill() {
    }

    public void addTariff(TariffRebill entry) {
    	tarrifs.add(entry);
    }

    public List<TariffRebill> getTariffs() {
            return tarrifs;
    }

  
}


