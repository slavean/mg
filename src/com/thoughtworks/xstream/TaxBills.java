package com.thoughtworks.xstream;

import java.util.ArrayList;
import java.util.List;

public class TaxBills {
    private List<Company> companies = new ArrayList<Company>();

    public TaxBills() {
    }

    public void addCompany(Company entry) {
    	companies.add(entry);
    }

    public List<Company> getCompanies() {
            return companies;
    }

    public Company findCompany(String acctId) {
    	Company foundC = null;
    	for (Company comp: companies)
    	{
    		if (comp.getLSPredprijatija().equals(acctId)) 
    		{
    			foundC = comp;
    			break;
    		}
    	}
      	return foundC;
    }
  
}


