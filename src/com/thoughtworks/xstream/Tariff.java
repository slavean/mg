package com.thoughtworks.xstream;

import com.ibm.icu.math.BigDecimal;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class Tariff {

	@XStreamOmitField
	private String AcctId_T;
	
    private BigDecimal Obiem_T;
    private BigDecimal CenaBezNDS_T;
    private BigDecimal ProcentNDS_T;
    private BigDecimal SummaBezNDS_T;
    private BigDecimal SummaNDS_T;
    private BigDecimal ObshhajaSumma_T;
    
	public Tariff(String acctId_T, BigDecimal obiem, BigDecimal cenaBezNDS, BigDecimal procentNDS,
			BigDecimal summaBezNDS, BigDecimal summaNDS, BigDecimal obshhajaSumma) {
		super();
		AcctId_T = acctId_T;
		Obiem_T = obiem;
		CenaBezNDS_T = cenaBezNDS;
		ProcentNDS_T = procentNDS;
		SummaBezNDS_T = summaBezNDS;
		SummaNDS_T = summaNDS;
		ObshhajaSumma_T = obshhajaSumma;
	}
	
	public Tariff(Tariff other) {
		super();
		this.AcctId_T = other.AcctId_T;
		this.Obiem_T = new BigDecimal(other.Obiem_T.toString());
		this.CenaBezNDS_T = new BigDecimal(other.CenaBezNDS_T.toString());
		this.ProcentNDS_T = new BigDecimal(other.ProcentNDS_T.toString());
		this.SummaBezNDS_T = new BigDecimal(other.SummaBezNDS_T.toString());
		this.SummaNDS_T = new BigDecimal(other.SummaNDS_T.toString());
		this.ObshhajaSumma_T = new BigDecimal(other.ObshhajaSumma_T.toString());
	}

	public BigDecimal getObiem() {
		return Obiem_T;
	}

	public void setObiem(BigDecimal obiem) {
		Obiem_T = obiem;
	}

	public BigDecimal getCenaBezNDS() {
		return CenaBezNDS_T;
	}

	public void setCenaBezNDS(BigDecimal cenaBezNDS) {
		CenaBezNDS_T = cenaBezNDS;
	}

	public BigDecimal getProcentNDS() {
		return ProcentNDS_T;
	}

	public void setProcentNDS(BigDecimal procentNDS) {
		ProcentNDS_T = procentNDS;
	}

	public BigDecimal getSummaBezNDS() {
		return SummaBezNDS_T;
	}

	public void setSummaBezNDS(BigDecimal summaBezNDS) {
		SummaBezNDS_T = summaBezNDS;
	}

	public BigDecimal getSummaNDS() {
		return SummaNDS_T;
	}

	public void setSummaNDS(BigDecimal summaNDS) {
		SummaNDS_T = summaNDS;
	}

	public BigDecimal getObshhajaSumma() {
		return ObshhajaSumma_T;
	}

	public void setObshhajaSumma(BigDecimal obshhajaSumma) {
		ObshhajaSumma_T = obshhajaSumma;
	}

	public String getAcctId_T() {
		return AcctId_T;
	}

	public void setAcctId_T(String acctId_T) {
		AcctId_T = acctId_T;
	}
    
    
    
    
    
}
