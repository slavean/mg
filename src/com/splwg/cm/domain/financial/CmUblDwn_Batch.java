package com.splwg.cm.domain.financial;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.ibm.icu.math.BigDecimal;
import com.splwg.base.api.batch.JobWork;
import com.splwg.base.api.batch.RunAbortedException;
import com.splwg.base.api.batch.StandardCommitStrategy;
import com.splwg.base.api.batch.ThreadAbortedException;
import com.splwg.base.api.batch.ThreadExecutionStrategy;
import com.splwg.base.api.batch.ThreadWorkUnit;
import com.splwg.base.api.batch.ThreadWorker;
import com.splwg.base.api.datatypes.Date;
import com.splwg.base.api.sql.PreparedStatement;
import com.splwg.base.api.sql.SQLResultRow;
import com.splwg.base.domain.common.language.Language_Id;
import com.splwg.base.domain.common.lookup.LookupField_Id;
import com.splwg.base.domain.common.lookup.LookupValue_Id;
import com.splwg.base.domain.common.lookup.LookupValue_LanguageId;
import com.splwg.ccb.domain.customerinfo.account.entity.Account_Id;
import com.splwg.cm.domain.admin.msg.Md_072_01_MessageRepository;
import com.splwg.cm.domain.common.CmApiFeatureConfigurationWrapper;
import com.splwg.shared.common.ApplicationError;
import com.splwg.shared.logging.Logger;
import com.splwg.shared.logging.LoggerFactory;
import com.thoughtworks.xstream.Address;
import com.thoughtworks.xstream.Peni;
import com.thoughtworks.xstream.Rebill;
import com.thoughtworks.xstream.ServicePoint;
import com.thoughtworks.xstream.Tariff;
import com.thoughtworks.xstream.TariffRebill;
import com.thoughtworks.xstream.Tariffs;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.TaxBills; //НалоговыеНакладные 
import com.thoughtworks.xstream.Company; //Предприятие
import com.thoughtworks.xstream.ibmBigDecimalConverter;
import com.thoughtworks.xstream.converters.basic.BigDecimalConverter;

/**
 * @batch CMUBLDWN
 * @batchDescription "ENG" "Выгрузка счетов ЮЛ" "Процесс осуществляет формирование и выгрузку XML-файла с информацией о потреблении и начислениях ЮЛ за отчетный месяц"
 * @batchDescription "RUS" "Выгрузка счетов ЮЛ" "Процесс осуществляет формирование и выгрузку XML-файла с информацией о потреблении и начислениях ЮЛ за отчетный месяц"
 * @batchDescription "RUM" "Выгрузка счетов ЮЛ" "Процесс осуществляет формирование и выгрузку XML-файла с информацией о потреблении и начислениях ЮЛ за отчетный месяц"
 * @batchParameter 10 "WFM" "CMUBLDWN" Y
 * @batchParameterDescription "ENG" "Настройка функциональности" "Настройка функциональности"
 * @batchParameterDescription "RUS" "Настройка функциональности" "Настройка функциональности"
 * @batchParameterDescription "RUM" "Настройка функциональности" "Настройка функциональности"
 * @batchParameter 20 "OTDELEN" "" N
 * @batchParameterDescription "ENG" "Код подразделения" "Код подразделения"
 * @batchParameterDescription "RUS" "Код подразделения" "Код подразделения"
 * @batchParameterDescription "RUM" "Код подразделения" "Код подразделения"
 * @batchParameter 30 "LANG" "RUM" Y
 * @batchParameterDescription "ENG" "Язык" "Язык"
 * @batchParameterDescription "RUS" "Язык" "Язык"
 * @batchParameterDescription "RUM" "Язык" "Язык"
 * @batchParameter 40 "SER" "MG" Y
 * @batchParameterDescription "ENG" "Серия накладных" "Серия накладных"
 * @batchParameterDescription "RUS" "Серия накладных" "Серия накладных"
 * @batchParameterDescription "RUM" "Серия накладных" "Серия накладных"
 * @batchParameter 50 "START_NUMBER" "" Y
 * @batchParameterDescription "ENG" "Начальный номер накладных" "Начальный номер накладных"
 * @batchParameterDescription "RUS" "Начальный номер накладных" "Начальный номер накладных"
 * @batchParameterDescription "RUM" "Начальный номер накладных" "Начальный номер накладных"
 * @batchParameter 60 "END_NUMBER"  "" Y
 * @batchParameterDescription "ENG" "Конечный номер накладных" "Конечный номер накладных"
 * @batchParameterDescription "RUS" "Конечный номер накладных" "Конечный номер накладных"
 * @batchParameterDescription "RUM" "Конечный номер накладных" "Конечный номер накладных"
 * @batchParameter 70 "SER1" "" N
 * @batchParameterDescription "ENG" "Запасная серия накладных" "Запасная серия накладных"
 * @batchParameterDescription "RUS" "Запасная серия накладных" "Запасная серия накладных"
 * @batchParameterDescription "RUM" "Запасная серия накладных" "Запасная серия накладных" 
 * @batchParameter 80 "START_NUMBER1" "" N
 * @batchParameterDescription "ENG" "Запасной начальный номер накладных" "Запасной начальный номер накладных"
 * @batchParameterDescription "RUS" "Запасной начальный номер накладных" "Запасной начальный номер накладных"
 * @batchParameterDescription "RUM" "Запасной начальный номер накладных" "Запасной начальный номер накладных" 
 * @batchParameter 90 "END_NUMBER1" "" N
 * @batchParameterDescription "ENG" "Запасной конечный номер накладных" "Запасной конечный номер накладных"
 * @batchParameterDescription "RUS" "Запасной конечный номер накладных" "Запасной конечный номер накладных"
 * @batchParameterDescription "RUM" "Запасной конечный номер накладных" "Запасной конечный номер накладных" 
 * @batchParameter 140 "TEST" "Y" Y
 * @batchParameterDescription "ENG" "TEST" "TEST (потом убрать)"
 * @batchParameterDescription "RUS" "TEST" "TEST (потом убрать)"
 * @batchParameterDescription "RUM" "TEST" "TEST (потом убрать)"
 * 
 * @author vmoglan
 * @BatchJob (modules = { }, 
 * rerunnable = false, 
 * multiThreaded = false,
 * softParameters = { @BatchJobSoftParameter (name = WFM, required = true, type = string)  
 * , @BatchJobSoftParameter (name = OTDELEN, required = false, type = string)  
 * , @BatchJobSoftParameter (name = LANG, required = true, type = string)
 * , @BatchJobSoftParameter (name = SER, required = true, type = string)
 * , @BatchJobSoftParameter (name = START_NUMBER, required = true, type = string)
 * , @BatchJobSoftParameter (name = END_NUMBER, required = true, type = string)
 * , @BatchJobSoftParameter (name = SER1, required = false, type = string)
 * , @BatchJobSoftParameter (name = START_NUMBER1, required = false, type = string)
 * , @BatchJobSoftParameter (name = END_NUMBER1, required = false, type = string)
 * , @BatchJobSoftParameter (name = TEST, required = false, type = string) 
 * } 
 * )
 */

public class CmUblDwn_Batch extends CmUblDwn_Batch_Gen {



	static Logger logger = LoggerFactory.getLogger(CmUblDwn_Batch.class);
	public static boolean test = false;
	public static boolean showLog = true;
	// static int nrErrors = 0;
	private static String SATP;
	private static String SAAC;
	private static String CUST;
	private static String BSST;
	private static String BFLG;
	private static String BNKL;
	private static String DGNB;
	private static String DGDT;
	private static String FISK;
	private static String NDSC;
	private static String OTV;
	private static String DOVN;
	private static String DOVD;
	private static String NLG;
	private static String TRF;
	private static String ISTR;
	private static String PNDS;
	private static String OTD;
	private static String CRTP;
//	private static String SACH;
	private static String PENI;
	private static String SER;
	private static String START_NUMBER;
	private static String END_NUMBER;
	
	private static BigInteger startNumber;
	private static BigInteger endNumber;	

	private static String SER1;
	private static String START_NUMBER1;
	private static String END_NUMBER1;

	private static BigInteger startNumber1;
	private static BigInteger endNumber1;	

	private static String wfm;
	private static boolean usedSER = false;

	@Override
	public Class<? extends ThreadWorker> getThreadWorkerClass() {
		// TODO Auto-generated method stub
		return CmUblDwn_BatchhWorker.class;
	}

	
	
	@Override
	public void validateSoftParameters(boolean isNewRun) {
		test = (getParameters().getTEST().toUpperCase().equals("Y") ? true : false);
		
		wfm = getParameters().getWFM().toUpperCase();
		SER = getParameters().getSER().toUpperCase();
		START_NUMBER = getParameters().getSTART_NUMBER();
		END_NUMBER = getParameters().getEND_NUMBER();
		
		startNumber = new BigInteger(START_NUMBER);
		endNumber = new BigInteger(END_NUMBER);
		// String otdelen = getParameters().getOTDELEN().toUpperCase();
		// String lang = getParameters().getLANG().toUpperCase();
		String path = "";
		// List<String> list_values;
		List<String> list_errors;

		List<String> satp_values = new ArrayList(), saac_values = new ArrayList(), cust_values = new ArrayList(), 
				bsst_values = new ArrayList(), bflg_values = new ArrayList(), bnkl_values = new ArrayList(), 
				dgnb_values = new ArrayList(), dgdt_values = new ArrayList(), fisk_values = new ArrayList(), ndsc_values = new ArrayList(), 
				otv_values = new ArrayList(), dovn_values = new ArrayList(), dovd_values = new ArrayList(), nlg_values = new ArrayList(), 
				trf_values = new ArrayList(), istr_values = new ArrayList(), pnds_values = new ArrayList(), otd_values = new ArrayList(), 				
				crtp_values = new ArrayList(), sach_values = new ArrayList(), peni_values = new ArrayList();

		// 1.1. Если настройка функциональности, указанная в WFM, не найдена
		if (!CmApiFeatureConfigurationWrapper.checkFeatureExistance(wfm)) {
			addError(Md_072_01_MessageRepository.noFN(wfm));

		}

		// 1.2. Если у настройки функциональности, указанной в WFM, не найден
		// параметр PATH
		try {
			path = CmApiFeatureConfigurationWrapper.getOption(wfm, "PATH");

		} catch (Exception E) {
			addError(Md_072_01_MessageRepository.noParamFN("PATH", wfm));
		}

		if (!test) {
			// 1.3. Если путь, заданный в значении параметра PATH настройки
			// функциональности, указанной в WFM, не существует
			if (!new File(path).exists()) {
				addError(Md_072_01_MessageRepository.noPath(path));
			}

		}

		// 1.4. Если у настройки функциональности, указанной в WFM, не
		// найден параметр SATP:

		list_errors = CheckExistingParams(
				wfm,
				"SATP",
				"select sat.SA_TYPE_CD as VALUE from CI_SA_TYPE sat where sat.SA_TYPE_CD = :SATP",
				satp_values);
//		if (list_errors == null) return null;

		// 1.5. Если в таблице типов РДО не найдена запись по условию:
		// CI_SA_TYPE. SA_TYPE_CD = значение параметра SATP (проверять каждый
		// параметр) настройки функциональности, указанной в WFM

		if (list_errors.size() > 0) 
		{
			if (!test) addError(Md_072_01_MessageRepository.noSA(list_errors.toString()));
		}

		// ---

		// 1.6. Если у настройки функциональности, указанной в WFM, не найден
		// параметр SAAC

		list_errors = CheckExistingParams(
				wfm,
				"SAAC",
				"select lk.FIELD_VALUE as VALUE from CI_LOOKUP lk where lk.FIELD_NAME='SA_STATUS_FLG' and lk.FIELD_VALUE = :SAAC",
				saac_values);
//		if (list_errors == null)	return null;

		// 1.7. Если в системном справочнике для поля SA_STATUS_FLG не найдено
		// хотя бы одно значение, определенное в параметре SAAC (Проверять все
		// значения) настройки функциональности, указанной в WFM

		if (list_errors.size() > 0) {
			addError(Md_072_01_MessageRepository.noFieldValue("SA_STATUS_FLG",
					list_errors.toString()));
		}


		// 1.8. Если у настройки функциональности, указанной в WFM, не найден
		// параметр CUST:

		list_errors = CheckExistingParams(
				wfm,
				"CUST",
				"select cust.CUST_CL_CD as VALUE from CI_CUST_CL cust where cust.CUST_CL_CD = :CUST",
				cust_values);
//		if (list_errors == null)		return null;

		// 1.9. Если в таблице категорий абонентов не найдена запись по условию:
		// CI_CUST_CL.CUST_CL_CD = значение параметра CUST (проверять каждый
		// параметр) настройки функциональности, указанной в WFM
		if (!test) {
			if (list_errors.size() > 0) {
				addError(Md_072_01_MessageRepository.noCustCategory(list_errors
						.toString()));
			}
		}
		// ---

		// 1.10. Если у настройки функциональности, указанной в WFM, не найден
		// параметр BSST

		list_errors = CheckExistingParams(
				wfm,
				"BSST",
				"select lk.FIELD_VALUE as VALUE from CI_LOOKUP lk where lk.FIELD_NAME='BSEG_STAT_FLG' and lk.FIELD_VALUE = :BSST",
				bsst_values);
//		if (list_errors == null)	return null;

		// 1.11. Если в системном справочнике для поля BSEG_STAT_FLG не найдено
		// хотя бы одно значение, определенное в параметре BSST (Проверять все
		// значения) настройки функциональности, указанной в WFM

		if (list_errors.size() > 0) {
			addError(Md_072_01_MessageRepository.noFieldValue("BSEG_STAT_FLG",
					list_errors.toString()));
		}

		// ---

		// 1.12. Если у настройки функциональности, указанной в WFM, не найден
		// параметр BFLG

		list_errors = CheckExistingParams(
				wfm,
				"BFLG",
				"select lk.FIELD_VALUE as VALUE from CI_LOOKUP lk where lk.FIELD_NAME='USAGE_FLG' and lk.FIELD_VALUE = :BFLG",
				bflg_values);
//		if (list_errors == null)	return null;

		// 1.13. Если в системном справочнике для поля USAGE_FLG не найдено хотя
		// бы одно значение, определенное в параметре BFLG (Проверять все
		// значения) настройки функциональности, указанной в WFM

		if (list_errors.size() > 0) {
			addError(Md_072_01_MessageRepository.noFieldValue("USAGE_FLG",
					list_errors.toString()));

		}

		// ---

		// 1.14. Если у настройки функциональности, указанной в WFM, не найден
		// параметр BNKL

		list_errors = CheckExistingParams(
				wfm,
				"BNKL",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'ACCT' and cce.CHAR_TYPE_CD = :BNKL",
				bnkl_values);
//		if (list_errors == null)			return null;

		// 1.15. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра BNKL настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'ACCT'
		if (!test) {
			if (list_errors.size() > 0) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"BNKL", list_errors.toString(), "ACCT"));

			}
		}

		// ---

		// 1.16. Если у настройки функциональности, указанной в WFM, не найден
		// параметр DGNB

		list_errors = CheckExistingParams(
				wfm,
				"DGNB",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'ACCT' and cce.CHAR_TYPE_CD = :DGNB",
				dgnb_values);
//		if (list_errors == null)	return null;

		// 1.17. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра DGNB настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'ACCT'

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"DGNB", list_errors.toString(), "ACCT"));
			}
		}

		// ---

		// 1.18. Если у настройки функциональности, указанной в WFM, не найден
		// параметр DGDT

		list_errors = CheckExistingParams(
				wfm,
				"DGDT",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'ACCT' and cce.CHAR_TYPE_CD = :DGDT",
				dgdt_values);
//		if (list_errors == null)			return null;

		// 1.19. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра DGDT настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'ACCT'

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"DGDT", list_errors.toString(), "ACCT"));
			}
		}

		// ---

		// 1.20. Если у настройки функциональности, указанной в WFM, не найден
		// параметр FISK

		list_errors = CheckExistingParams(
				wfm,
				"FISK",
				"select cit.ID_TYPE_CD as VALUE from CI_ID_TYPE cit where cit.ID_TYPE_CD = :FISK",
				fisk_values);
//		if (list_errors == null) return null;

		// 1.21. Если в таблице CI_ID_TYPE не найдена запись по условию:
		// CI_ID_TYPE.ID_TYPE_CD = значение параметра FISK настройки
		// функциональности, указанной в WFM

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noTypeIdenitity("FISK",
						list_errors.toString()));
			}
		}

		// ---

		// 1.22. Если у настройки функциональности, указанной в WFM, не найден
		// параметр NDSC

		list_errors = CheckExistingParams(
				wfm,
				"NDSC",
				"select cit.ID_TYPE_CD as VALUE from CI_ID_TYPE cit where cit.ID_TYPE_CD = :NDSC",
				ndsc_values);
//		if (list_errors == null)	return null;

		// 1.23. Если в таблице CI_ID_TYPE не найдена запись по условию:
		// CI_ID_TYPE.ID_TYPE_CD = значение параметра NDSC настройки
		// функциональности, указанной в WFM

		if (list_errors.size() > 0) {
			if (!test) {
//!!!вернуть				
				addError(Md_072_01_MessageRepository.noTypeIdenitity("NDSC", list_errors.toString()));
			}
		}

		// ---

		// 1.24. Если у настройки функциональности, указанной в WFM, не найден
		// параметр OTV

		list_errors = CheckExistingParams(
				wfm,
				"OTV",
				"select cart.ACCT_REL_TYPE_CD as VALUE from CI_ACCT_REL_TYP cart where cart.ACCT_REL_TYPE_CD = :OTV",
				otv_values);
//		if (list_errors == null)	return null;

		// 1.25. Если в таблице CI_ACCT_REL_TYPE не найдена запись по условию:
		// ACCT_REL_TYPE_CD = значение параметра OTV настройки функциональности,
		// указанной в WFM

		if (list_errors.size() > 0) {
			if (!test) {
//!!!вернуть				
				addError(Md_072_01_MessageRepository.noTypeRelation("OTV",	list_errors.toString()));

			}
		}

		// ---

		// 1.26. Если у настройки функциональности, указанной в WFM, не найден
		// параметр DOVN

		list_errors = CheckExistingParams(
				wfm,
				"DOVN",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'PERS' and cce.CHAR_TYPE_CD = :DOVN",
				dovn_values);
//		if (list_errors == null) return null;

		// 1.27. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра DOVN настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'PERS':

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"DOVN", list_errors.toString(), "PERS"));

			}
		}

		// ---

		// 1.28. Если у настройки функциональности, указанной в WFM, не найден
		// параметр DOVD

		list_errors = CheckExistingParams(
				wfm,
				"DOVD",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'PERS' and cce.CHAR_TYPE_CD = :DOVD",
				dovd_values);
//		if (list_errors == null)	return null;

		// 1.29. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра DOVD настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'PERS'

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"DOVD", list_errors.toString(), "PERS"));

			}
		}

		// ---

		// 1.30. Если у настройки функциональности, указанной в WFM, не найден
		// параметр NLG

		list_errors = CheckExistingParams(
				wfm,
				"NLG",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'BSCL' and cce.CHAR_TYPE_CD = :NLG",
				nlg_values);
//		if (list_errors == null)			return null;

		// 1.31. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра NLG настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'BSCL'

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject("NLG",
						list_errors.toString(), "BSCL"));

			}
		}

		// ---

		// 1.33. Если у настройки функциональности, указанной в WFM, не найден
		// параметр TRF

		list_errors = CheckExistingParams(
				wfm,
				"TRF",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'BSCL' and cce.CHAR_TYPE_CD = :TRF",
				trf_values);
//		if (list_errors == null) return null;

		// 1.34. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра TRF настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'BSCL'

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject("TRF",
						list_errors.toString(), "BSCL"));

			}
		}

		// ---

		// 1.35. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра TRF настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'C1AL'

		list_errors = CheckExistingParams(
				wfm,
				"TRF",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'C1AL' and cce.CHAR_TYPE_CD = :TRF",
				trf_values);
//		if (list_errors == null)	return null;
		
		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject("TRF",
						list_errors.toString(), "C1AL"));
			}
		}

		// ---

		// 1.36. Если у настройки функциональности, указанной в WFM, не найден
		// параметр ISTR

		list_errors = CheckExistingParams(
				wfm,
				"ISTR",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'BSCL' and cce.CHAR_TYPE_CD = :ISTR",
				istr_values);
//		if (list_errors == null)	return null;

		// 1.37. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра ISTR настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'BSCL'

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"ISTR", list_errors.toString(), "BSCL"));
			}
		}

		// ---

		// 1.38. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра ISTR настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'C1AL'

		list_errors = CheckExistingParams(
				wfm,
				"ISTR",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'C1AL' and cce.CHAR_TYPE_CD = :ISTR",
				istr_values);
//		if (list_errors == null)return null;
		
		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"ISTR", list_errors.toString(), "C1AL"));
			}
		}

		// ---

		// 1.39. Если у настройки функциональности, указанной в WFM, не найден
		// параметр PNDS

		list_errors = CheckExistingParams(
				wfm,
				"PNDS",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'BSCL' and cce.CHAR_TYPE_CD = :PNDS",
				pnds_values);
//		if (list_errors == null) return null;

		// 1.40. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра PNDS настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'BSCL'

		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"PNDS", list_errors.toString(), "BSCL"));
			}
		}

		// 1.41. Если в таблице CI_CHAR_ENTITY не найдена запись по условию:
		// CHAR_TYPE_CD = значение параметра PNDS настройки функциональности,
		// указанной в WFM и CHAR_ENTITY_FLG = 'C1AL'

		list_errors = CheckExistingParams(
				wfm,
				"PNDS",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'C1AL' and cce.CHAR_TYPE_CD = :PNDS",
				pnds_values);
//		if (list_errors == null) return null;
		
		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject(
						"PNDS", list_errors.toString(), "C1AL"));
			}
		}

		// 1.42.	Если у настройки функциональности, указанной в WFM, не найден параметр CRTP:

		list_errors = CheckExistingParams(
				wfm,
				"CRTP",
				"select at.ADJ_TYPE_CD  as VALUE from CI_ADJ_TYPE at where at.ADJ_TYPE_CD  = :CRTP",
				crtp_values);
//		if (list_errors == null) 	return null;
		
		// 1.43.	Если в таблице CI_ADJ_TYPE не найдена запись по условию: ADJ_TYPE_CD = значение параметра CRTP  настройки функциональности, указанной в WFM
		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noTypeCorr(list_errors.toString())); 				
			}
		}

/*		
		// 1.44.	Если у настройки функциональности, указанной в WFM, не найден параметр SACH:

		list_errors = CheckExistingParams(
				wfm,
				"SACH",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'SATY' and cce.CHAR_TYPE_CD = :SACH",
				sach_values);
//		if (list_errors == null)	return null;
		
		// 1.45.	Если в таблице CI_CHAR_ENTITY не найдена запись по условию: CHAR_TYPE_CD = значение параметра SACH настройки функциональности, указанной в WFM и CHAR_ENTITY_FLG = 'SATY':
		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject("SACH", list_errors.toString(), "SATY")); 
				//return null;
			}
		}
		
		*/
		
		// 1.44.	Если у настройки функциональности, указанной в WFM, не найден параметр PENI:

		list_errors = CheckExistingParams(
				wfm,
				"PENI",
				
				/*"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_VAL  cce where CHAR_VAL = :PENI and cce.CHAR_TYPE_CD = '"+ CmApiFeatureConfigurationWrapper.getOption(wfm, "PENI")+"'"*/
				
				""
				
				,
				peni_values);
//		if (list_errors == null)	return null;
		
		// 1.47.	Если в таблице CI_CHAR_VAL не найдена запись по условию: CHAR_TYPE_CD = значение параметра SACH настройки функциональности, указанной в WFM и CHAR_VAL = значение параметра PENI:
		if (list_errors.size() > 0) {
			if (!test) {
				//!!!вернуть				
				addError(Md_072_01_MessageRepository.noTypeCharForCharType(list_errors.toString(), CmApiFeatureConfigurationWrapper.getOption(wfm, "PENI"))); 
				//return null;
			}
		}

		
		// 1.46.	Если у настройки функциональности, указанной в WFM, не найден параметр OTD:
		
		list_errors = CheckExistingParams(
				wfm,
				"OTD",
				"select cce.CHAR_TYPE_CD as VALUE from CI_CHAR_ENTITY cce where CHAR_ENTITY_FLG = 'ACCT' and cce.CHAR_TYPE_CD = :OTD",
				otd_values);
//		if (list_errors == null)	return null;
		// 1.47.	Если в таблице CI_CHAR_ENTITY не найдена запись по условию: CHAR_TYPE_CD = значение параметра OTD настройки функциональности, указанной в WFM и CHAR_ENTITY_FLG = 'ACCT':		
		if (list_errors.size() > 0) {
			if (!test) {
				addError(Md_072_01_MessageRepository.noChartypeForObject("OTD",
						list_errors.toString(), "ACCT")); 
			}
		}
		
		// 1.48.	Если в заполнено любое из полей SER1, START_NUMBER1, END_NUMBER1, то проверить факт заполнения остальных полей. В случае, если одно из них не заполнено:
		List<String> err_list = new ArrayList();
		
		SER1 = getParameters().getSER1();
		START_NUMBER1 = getParameters().getSTART_NUMBER1();
		END_NUMBER1 = getParameters().getEND_NUMBER1();
		if (isBlankOrNull(SER1)) err_list.add("SER1");
		if (isBlankOrNull(START_NUMBER1)) err_list.add("START_NUMBER1");
		if (isBlankOrNull(END_NUMBER1)) err_list.add("END_NUMBER1");
		if ((err_list.size()!=0) && (err_list.size()!=3))
		{
				addError(Md_072_01_MessageRepository.fieldsNotFilled(err_list.toString()));
		}
		if (err_list.size()==0)
		{
			startNumber1 = new BigInteger(START_NUMBER1);
			endNumber1 = new BigInteger(START_NUMBER1);
		}
		else
		{
			startNumber1 = BigInteger.ZERO;
			endNumber1 = BigInteger.ZERO;
		}
		
		SATP = ListToIn(satp_values);
		SAAC = ListToIn(saac_values);
		CUST = ListToIn(cust_values);
		BSST = ListToIn(bsst_values);
		BFLG = ListToIn(bflg_values);
		BNKL = ListToIn(bnkl_values);
		DGNB = ListToIn(dgnb_values);
		DGDT = ListToIn(dgdt_values);
		FISK = ListToIn(fisk_values);
		NDSC = ListToIn(ndsc_values);
		OTV  = ListToIn(otv_values);
		DOVN = ListToIn(dovn_values);
		DOVD = ListToIn(dovd_values);
		NLG  = ListToIn(nlg_values);
		TRF  = ListToIn(trf_values);
		ISTR = ListToIn(istr_values);
		PNDS = ListToIn(pnds_values);
		OTD  = ListToIn(otd_values);
		CRTP = ListToIn(crtp_values);
//		SACH = ListToIn(sach_values);
		PENI = ListToIn(peni_values);
	}
	
	
	@Override
	public JobWork getJobWork() {

		List<ThreadWorkUnit> units = new ArrayList<ThreadWorkUnit>();


		
		ThreadWorkUnit unit = new ThreadWorkUnit();
/*
		unit.addSupplementalData("SATP", ListToIn(satp_values));
		unit.addSupplementalData("SAAC", ListToIn(saac_values));
		unit.addSupplementalData("CUST", ListToIn(cust_values));
		unit.addSupplementalData("BSST", ListToIn(bsst_values));
		unit.addSupplementalData("BFLG", ListToIn(bflg_values));
		unit.addSupplementalData("BNKL", ListToIn(bnkl_values));
		unit.addSupplementalData("DGNB", ListToIn(dgnb_values));
		unit.addSupplementalData("DGDT", ListToIn(dgdt_values));
		unit.addSupplementalData("FISK", ListToIn(fisk_values));
		unit.addSupplementalData("NDSC", ListToIn(ndsc_values));
		unit.addSupplementalData("OTV", ListToIn(otv_values));
		unit.addSupplementalData("DOVN", ListToIn(dovn_values));
		unit.addSupplementalData("DOVD", ListToIn(dovd_values));
		unit.addSupplementalData("NLG", ListToIn(nlg_values));
		unit.addSupplementalData("TRF", ListToIn(trf_values));
		unit.addSupplementalData("ISTR", ListToIn(istr_values));
		unit.addSupplementalData("PNDS", ListToIn(pnds_values));
		unit.addSupplementalData("OTD", ListToIn(otd_values));
		unit.addSupplementalData("CRTP", ListToIn(crtp_values));

		unit.addSupplementalData("SACH", ListToIn(sach_values));
		unit.addSupplementalData("PENI", ListToIn(peni_values));
		*/
		

		
		units.add(unit);
		if (test) {
			// break;
		}

		// XStream xstream = new XStream();

		return createJobWorkForThreadWorkUnitList(units);
	}

	String ListToIn(List<String> list) {
		String s = "";
		for (String param : list) {
			if (isBlankOrNull(s))
				s = s + "\'" + param + "\'";
			else
				s = s + ", \'" + param + "\'";
		}

		return s;

	}

	public static class CmUblDwn_BatchhWorker extends CmUblDwn_BatchWorker_Gen {



		private String lastNakl = "";
		private BigInteger countNakl = BigInteger.ZERO;

		@Override
		public ThreadExecutionStrategy createExecutionStrategy() {
			// TODO Auto-generated method stub
			return new StandardCommitStrategy(this);
		}

		@SuppressWarnings("deprecation")
		@Override
		protected boolean executeWorkUnit(ThreadWorkUnit unit)
				throws ThreadAbortedException, RunAbortedException {

			// logger.info("tra-la-la" + perIdStr);

			// int maxErrors = getParameters().getMAX_ERRORS().intValue();

			//String wfm = getParameters().getWFM().toUpperCase();
			String otdelen="";
			if (getParameters().getOTDELEN() !=null)	 otdelen = getParameters().getOTDELEN().toUpperCase();
						
			String lang = getParameters().getLANG().toUpperCase();
		
			String UOM = CmApiFeatureConfigurationWrapper.getOption(wfm, "UOM");

			usedSER = false;
			
			String query = "";

			StringBuilder sb = new StringBuilder();


			Date v_EndDt = getParameters().getProcessDate();
			Date v_StartDt = v_EndDt.addDays(-v_EndDt.getDay() + 1);
			

			LookupField_Id f_id = new LookupField_Id("F1MONTH_ABBR");

			String month = String.valueOf(v_EndDt.getMonth());
			if (v_EndDt.getMonth() < 10)
				month = "0" + month;

			LookupValue_Id Ipp = new LookupValue_Id(f_id, month);
			// LookupValue_DTO Ipp_v =(LookupValue_DTO) Ipp.getEntity();

			LookupValue_LanguageId ll = new LookupValue_LanguageId(Ipp,
					new Language_Id("RUM"));

			// logger.info("LookupValue_DTO = ["+Ipp_v.getLanguageLongDescription()+"]");

			logger.info("LookupValue_LanguageId = ["
					+ ll.getLanguageEntity().getLongDescription() + "]");

			String fileName = ll.getLanguageEntity().getLongDescription();

			String path = CmApiFeatureConfigurationWrapper.getOption(wfm,
					"PATH");
			// if (test) path = "C:\\u01\\";
			if (!isBlankOrNull(otdelen))
				fileName = fileName + "_" + otdelen;
			fileName = fileName + ".xml";

			logger.info("FILENAME = [" + fileName + "]");
			logger.info("PATH = [" + path + "]");

			File F = new File(path + fileName);
			try {
				if (!test) {
				if (!F.createNewFile()) {
					addError(Md_072_01_MessageRepository.errorCreatingFile(
							path, fileName));
					return false;
					
				}
				}

			} catch (IOException e) {
				addError(Md_072_01_MessageRepository.errorCreatingFile(path,
						fileName));

				e.printStackTrace();
				return false;

			}

		
			sb.setLength(0);

			if (test) {
				sb.append("select ROWNUM as LSPredprijatija , \n");
				sb.append("/*SUM(CHRG.BILL_SQ)*/ 10 as Objem, \n");
				sb.append("/*SUM(CHRG.CALC_AMT)*/ 20 as SummaBezNDS, \n");
				sb.append("/*TRF.ADHOC_CHAR_VAL*/ 30 as CenaBezNDS, \n");
				sb.append("/*SUM(NDS.CALC_AMT)*/ 40 as SummaNDS, \n");
				sb.append("99.44 as ObshhajaSumma, \n");
				sb.append("/*NDSTRF.ADHOC_CHAR_VAL*/ 50 as ProcentNDS from CI_CHAR_TYPE where ROWNUM<=5 \n");
			} else {
				sb.append("select BILL.ACCT_ID as LSPredprijatija, \n");
				sb.append("SUM(CHRG.BILL_SQ) as Objem, \n");
				sb.append("SUM(CHRG.CALC_AMT) as SummaBezNDS, \n");
				sb.append("case  \n"
						+ "when trim(TRF.ADHOC_CHAR_VAL) is null or length(trim(TRF.ADHOC_CHAR_VAL))=0 then 0  \n"
						+ "else to_number(trim(replace(TRF.ADHOC_CHAR_VAL,',','.')),'99999999.999999999')  \n"
						+ "end as CenaBezNDS, \n");
				sb.append("SUM(NDS.CALC_AMT) as SummaNDS, \n");
				sb.append("case \n"
						+ "when trim(NDSTRF.ADHOC_CHAR_VAL) is null or length(trim(NDSTRF.ADHOC_CHAR_VAL))=0 then 0  \n"
						+ "else to_number(trim(replace(NDSTRF.ADHOC_CHAR_VAL,',','.')),'99999999.999999999' )  \n"
						+ "end*100 as ProcentNDS, \n");
				sb.append("SUM(CHRG.CALC_AMT) + SUM(NDS.CALC_AMT) as ObshhajaSumma \n");
				sb.append("from ( \n");
				
				sb.append("select DISTINCT(BSEG_ID) from CI_FT \n");
				sb.append("join CI_BSEG on CI_BSEG.BSEG_STAT_FLG in (:BSST) and CI_BSEG.END_DT between :v_StartDt and :v_EndDt and CI_FT.SIBLING_ID = CI_BSEG.BSEG_ID \n");			
				sb.append("join CI_BILL on CI_BILL.BILL_ID = CI_BSEG.BILL_ID \n");
				if (!isBlankOrNull(otdelen))
					sb.append("join CI_ACCT_CHAR on (CI_ACCT_CHAR.ACCT_ID = CI_BILL.ACCT_ID) and (CI_ACCT_CHAR.CHAR_TYPE_CD in (:OTD)) "
							+ "and (CI_ACCT_CHAR.CHAR_VAL LIKE :OTDELEN)  and (CI_ACCT_CHAR.EFFDT = (select MAX(EFFDT) from CI_ACCT_CHAR where CHAR_TYPE_CD in (:OTD))) \n");
				sb.append("WHERE CI_FT.FT_TYPE_FLG = 'BS' and CI_FT.ACCOUNTING_DT between :v_StartDt and :v_EndDt \n");

				sb.append(") T \n");
				sb.append("join CI_BSEG_CL_CHAR NLG on	NLG.BSEG_ID = T.BSEG_ID and NLG.CHAR_TYPE_CD in (:NLG) \n");
				sb.append("join CI_BSEG_CALC_LN CHRG on CHRG.BSEG_ID = NLG.BSEG_ID and	CHRG.HEADER_SEQ = NLG.HEADER_SEQ and CHRG.SEQNO = NLG.SEQNO \n");
				sb.append("join CI_BSEG_CL_CHAR TRF on NLG.BSEG_ID = TRF.BSEG_ID and TRF.CHAR_TYPE_CD in (:TRF) and NLG.HEADER_SEQ = TRF.HEADER_SEQ and NLG.SEQNO = TRF.SEQNO \n");
				sb.append("join CI_BSEG_CL_CHAR NDSCH on NDSCH.BSEG_ID = NLG.BSEG_ID and NDSCH.CHAR_TYPE_CD in (:ISTR) and "
						+ "TRIM(NDSCH.HEADER_SEQ) = TRIM(SUBSTR(NLG.ADHOC_CHAR_VAL, 1, INSTR(NLG.ADHOC_CHAR_VAL, ',')-1)) /* Значение до «,» */ "
						+ "and TRIM(NDSCH.SEQNO) = TRIM(SUBSTR(NLG.ADHOC_CHAR_VAL, INSTR(NLG.ADHOC_CHAR_VAL, ',')+1)) /* Значение после «,»*/  \n");
				sb.append("join CI_BSEG_CALC_LN NDS on	NDS.BSEG_ID = NLG.BSEG_ID and	NDS.HEADER_SEQ = NDSCH.HEADER_SEQ and 	NDS.SEQNO = NDSCH.SEQNO \n");
				sb.append("join CI_BSEG_CL_CHAR NDSTRF on NDSTRF.BSEG_ID = NLG.BSEG_ID and	NDSTRF.CHAR_TYPE_CD in (:PNDS) and	"
						+ "NDSTRF.HEADER_SEQ = NDSCH.HEADER_SEQ and NDSTRF.SEQNO = NDSCH.SEQNO \n");			
				sb.append("join CI_BSEG BS on BS.BSEG_ID = NLG.BSEG_ID \n");
				sb.append("join CI_BILL BILL on BILL.BILL_ID = BS.BILL_ID \n");
				sb.append("group by \n");
				sb.append("TRF.ADHOC_CHAR_VAL, NDSTRF.ADHOC_CHAR_VAL, BILL.ACCT_ID \n");
			}
			
			String queryTarif = sb.toString();
			
			queryTarif = queryTarif.replaceAll(":BSST", BSST);
			queryTarif = queryTarif.replaceAll(":NLG", NLG);
			queryTarif = queryTarif.replaceAll(":TRF", TRF);
			queryTarif = queryTarif.replaceAll(":ISTR", ISTR);
			queryTarif = queryTarif.replaceAll(":PNDS", PNDS);
			queryTarif = queryTarif.replaceAll(":TRF", TRF);
			queryTarif = queryTarif.replaceAll(":OTDELEN", "'" + otdelen + "%'");
			queryTarif = queryTarif.replaceAll(":OTD", OTD);

			TaxBills NN = new TaxBills();
			
			PreparedStatement qT = null;
			try {
				qT = createPreparedStatement(queryTarif);
				
				
				if (!test)
				
					qT.bindDate("v_EndDt", v_EndDt);
				if (!test)
					qT.bindDate("v_StartDt", v_StartDt);
 
				if (showLog)
					logger.info("qT = "+qT.toString());
				
				List<SQLResultRow> tarifList = qT.list();

				for (SQLResultRow rowT : tarifList) {
					String LSPredprijatija_T = rowT.getString("LSPREDPRIJATIJA");	
				
					BigDecimal Objem_T = rowT.getBigDecimal("OBJEM");
					BigDecimal SummaBezNDS_T = rowT.getBigDecimal("SUMMABEZNDS");
					BigDecimal CenaBezNDS_T = rowT.getBigDecimal("CENABEZNDS");
					BigDecimal SummaNDS_T = rowT.getBigDecimal("SUMMANDS");
					BigDecimal ProcentNDS_T = rowT.getBigDecimal("PROCENTNDS");
					BigDecimal ObshhajaSumma_T = rowT.getBigDecimal("OBSHHAJASUMMA");

					Tariff Tr = new Tariff(LSPredprijatija_T, Objem_T, CenaBezNDS_T,  ProcentNDS_T, SummaBezNDS_T, SummaNDS_T,	ObshhajaSumma_T);
					
					Company comp = NN.findCompany(LSPredprijatija_T);
					if (comp!=null) 
						comp.addTariff(Tr);
					else
					{
						comp = new Company("", "", "", LSPredprijatija_T, "","", "", "", "", "", "", "", "", "", "", "", null, null, null);
						NN.addCompany(comp);
					
						comp.addTariff(Tr);
					}
				}	
			}	
			 finally {
				if (qT != null)
					qT.close();
			}
					
			
			// Точки учета

					sb.setLength(0);

					if (test) {
						sb.append("select 1 as ACCT_ID, \n");
						sb.append("10*ROWNUM as SP_ID, \n");
						sb.append("'hello' as ADDRESS1, \n");
						sb.append("'sdsds' as BADGE_NBR, \n");
						sb.append("100 as START_REG_READING, \n");
						sb.append("300 as END_REG_READING, \n");
						sb.append("200 as FINAL_REG_QTY \n");
						sb.append("from CI_CHAR_TYPE where ROWNUM<=1 \n");
						sb.append("union \n");
						sb.append("select 2 as ACCT_ID, \n");
						sb.append("10*ROWNUM as SP_ID, \n");
						sb.append("'hello' as ADDRESS1, \n");
						sb.append("'sdsds' as BADGE_NBR, \n");
						sb.append("100 as START_REG_READING, \n");
						sb.append("300 as END_REG_READING, \n");
						sb.append("200 as FINAL_REG_QTY \n");
						sb.append("from CI_CHAR_TYPE where ROWNUM<=2 \n");
						sb.append("union \n");
						sb.append("select 3 as ACCT_ID, \n");
						sb.append("10*ROWNUM as SP_ID, \n");
						sb.append("'hello' as ADDRESS1, \n");
						sb.append("'sdsds' as BADGE_NBR, \n");
						sb.append("100 as START_REG_READING, \n");
						sb.append("300 as END_REG_READING, \n");
						sb.append("200 as FINAL_REG_QTY \n");
						sb.append("from CI_CHAR_TYPE where ROWNUM<=3 \n");
						sb.append("union \n");
						sb.append("select 4 as ACCT_ID, \n");
						sb.append("10*ROWNUM as SP_ID, \n");
						sb.append("'hello' as ADDRESS1, \n");
						sb.append("'sdsds' as BADGE_NBR, \n");
						sb.append("100 as START_REG_READING, \n");
						sb.append("300 as END_REG_READING, \n");
						sb.append("200 as FINAL_REG_QTY \n");
						sb.append("from CI_CHAR_TYPE where ROWNUM<=4 \n");
						sb.append("union \n");
						sb.append("select 5 as ACCT_ID, \n");
						sb.append("10*ROWNUM as SP_ID, \n");
						sb.append("'hello' as ADDRESS1, \n");
						sb.append("'sdsds' as BADGE_NBR, \n");
						sb.append("100 as START_REG_READING, \n");
						sb.append("300 as END_REG_READING, \n");
						sb.append("200 as FINAL_REG_QTY \n");
						sb.append("from CI_CHAR_TYPE where ROWNUM<=5 \n");
						
						
					} else {
						sb.append("select BILL.ACCT_ID, BSR.SP_ID, \n");
						sb.append("PREM.ADDRESS1, \n");
						sb.append("MTR.BADGE_NBR, \n");
						sb.append("BSR.START_REG_READING, \n");
						sb.append("BSR.END_REG_READING, \n");
						sb.append("BSR.FINAL_REG_QTY \n");
						sb.append("from ( \n");
						sb.append("select DISTINCT(BSEG_ID) from CI_BILL \n");
						sb.append("join CI_BSEG on CI_BSEG.BILL_ID = CI_BILL.BILL_ID and CI_BSEG.BSEG_STAT_FLG in (:BSST) and CI_BSEG.END_DT between :v_StartDt and :v_EndDt  \n");					
						if (!isBlankOrNull(otdelen))
							sb.append("join CI_ACCT_CHAR on (CI_ACCT_CHAR.ACCT_ID = CI_BILL.ACCT_ID) and (CI_ACCT_CHAR.CHAR_TYPE_CD in (:OTD)) "
									+ "and (CI_ACCT_CHAR.CHAR_VAL LIKE :OTDELEN)  and (CI_ACCT_CHAR.EFFDT = (select MAX(EFFDT) from CI_ACCT_CHAR where CHAR_TYPE_CD in (:OTD))) \n");
						
						sb.append("join CI_FT on CI_FT.SIBLING_ID = CI_BSEG.BSEG_ID and CI_FT.FT_TYPE_FLG = 'BS' and CI_FT.ACCOUNTING_DT between :v_StartDt and :v_EndDt \n");
						sb.append("where CI_BILL.BILL_ID = CI_BSEG.BILL_ID  \n");
						
						sb.append(") T \n");

						sb.append("join CI_BSEG_READ BSR on BSR.BSEG_ID = T.BSEG_ID and BSR.USAGE_FLG in (:BFLG) \n");
						sb.append("left join CI_REG_READ RR on BSR.END_REG_READ_ID = RR.REG_READ_ID \n");
						sb.append("left join CI_MR MR on MR.MR_ID = RR.MR_ID \n");
						sb.append("left join CI_MTR_CONFIG CFG on CFG.MTR_CONFIG_ID = MR.MTR_CONFIG_ID \n");
						sb.append("left join CI_MTR MTR on MTR.MTR_ID = CFG.MTR_ID \n");
						sb.append("join CI_SP SP on SP.SP_ID = BSR.SP_ID \n");
						sb.append("join CI_PREM PREM on PREM.PREM_ID = SP.PREM_ID \n");
						sb.append("join CI_BSEG BS on BS.BSEG_ID = BSR.BSEG_ID \n");
						sb.append("join CI_BILL BILL on BILL.BILL_ID = BS.BILL_ID \n");
					}
					String querySP = sb.toString();

					querySP = querySP.replaceAll(":BFLG", BFLG);
					querySP = querySP.replaceAll(":BSST", BSST);
					querySP = querySP.replaceAll(":OTDELEN", "'" + otdelen + "%'");
					querySP = querySP.replaceAll(":OTD", OTD);

					PreparedStatement qSP = null;
					try {
						qSP = createPreparedStatement(querySP);
						if (!test)
							qSP.bindDate("v_EndDt", v_EndDt);
						if (!test)
							qSP.bindDate("v_StartDt", v_StartDt);

						if (showLog)
							logger.info("qSP = "+qSP.toString());
						
						List<SQLResultRow> spList = qSP.list();

						for (SQLResultRow rowSP : spList) {
							String LSPredprijatija = rowSP.getString("ACCT_ID");	
							String TochkaUcheta = rowSP.getString("SP_ID");
							logger.info("Точка учета = "+TochkaUcheta);
							String AdresTU = rowSP.getString("ADDRESS1");
							String NomerPU = rowSP.getString("BADGE_NBR");
							String NachPokaz = rowSP
									.getString("START_REG_READING");
							String KonPokaz = rowSP
									.getString("END_REG_READING");
							BigDecimal Obiem = rowSP.getBigDecimal("FINAL_REG_QTY");

							ServicePoint SP = new ServicePoint(TochkaUcheta, AdresTU, NomerPU, NachPokaz, KonPokaz, Obiem, UOM);
							Company comp = NN.findCompany(LSPredprijatija);
							if (comp!=null) 
								comp.addPotreblenie(SP);

						}
					} finally {
						if (qSP != null)
							qSP.close();
					}
					// Конец ТочкаУчета					
					
			
					// Корректировки 1

					sb.setLength(0);

					if (test) {
						sb.append("SELECT 1 as ACCT_ID, \n");
						sb.append("/*SUM( \n");
						sb.append("CASE FT1.FT_TYPE_FLG \n");
						sb.append("WHEN 'AD' THEN CHRG.BILL_SQ --(Для ФТ с типом «Корректировка» значение объема газа берется со знаком «+») \n");
						sb.append("WHEN 'AX' THEN -CHRG.BILL_SQ --(Для ФТ с типом «Отмена корректировки» значение объема газа берется со знаком «-») \n");
						sb.append("END)*/44*ROWNUM AS Qty, \n");
						sb.append("/*SUM(CHRG.CALC_AMT)*/ 55*ROWNUM as TaxFreeCharge, \n");
						sb.append("/*TRF.ADHOC_CHAR_VAL*/10*ROWNUM as TaxFreePrice, \n");
						sb.append("/*SUM(NDS.CALC_AMT) */ 55*ROWNUM as TaxSum, \n");
						sb.append("/*NDSTRF.ADHOC_CHAR_VAL*100*/22*ROWNUM as Tax, \n");
						sb.append("6667*ROWNUM as Charge \n");
						sb.append("from CI_CHAR_TYPE where ROWNUM<=5 \n");
					} else {

						sb.append("SELECT FT1.ACCT_ID, \n");
						sb.append("SUM( \n");
						sb.append("CASE FT1.FT_TYPE_FLG \n");
						sb.append("WHEN 'AD' THEN CHRG.BILL_SQ --(Для ФТ с типом «Корректировка» значение объема газа берется со знаком «+») \n");
						sb.append("WHEN 'AX' THEN -CHRG.BILL_SQ --(Для ФТ с типом «Отмена корректировки» значение объема газа берется со знаком «-») \n");
						sb.append("END) AS Qty, \n");
						sb.append("SUM(CHRG.CALC_AMT) as TaxFreeCharge /* СуммаБезНДС */, \n");
						sb.append("case \n"
								+ "when trim(TRF.ADHOC_CHAR_VAL) is null or length(trim(TRF.ADHOC_CHAR_VAL))=0 then 0 \n"
								+ "else to_number(trim(replace(TRF.ADHOC_CHAR_VAL,',','.')),'99999999.999999999' \n)"
								+ "end as TaxFreePrice /* ЦенаБезНДС */, \n");
						sb.append("SUM(NDS.CALC_AMT) as TaxSum /* СуммаНДС */ , \n");
																
						sb.append("case \n"
								+ "when trim(NDSTRF.ADHOC_CHAR_VAL) is null or length(trim(NDSTRF.ADHOC_CHAR_VAL))=0 then 0 \n"
								+ "else to_number(trim(replace(NDSTRF.ADHOC_CHAR_VAL,',','.')),'99999999.999999999' ) \n"
								+ "end*100 as Tax /* ПроцентНДС */, \n");
						sb.append("SUM(CHRG.CALC_AMT) + SUM(NDS.CALC_AMT) as Charge /* ОбщаяСумма */\n");
					    sb.append("from ( \n");
						sb.append("SELECT DISTINCT SA.ACCT_ID, FT.SIBLING_ID, FT.FT_TYPE_FLG from CI_SA SA  \n");
						sb.append("join CI_FT FT on FT.SA_ID = SA.SA_ID and FT.FT_TYPE_FLG in ('AD','AX') and FT.ACCOUNTING_DT between :v_StartDt and :v_EndDt and FT.PARENT_ID in (:CRTP) \n");
						if (!isBlankOrNull(otdelen))
							sb.append("join CI_ACCT_CHAR on (CI_ACCT_CHAR.ACCT_ID = SA.ACCT_ID) and (CI_ACCT_CHAR.CHAR_TYPE_CD in (:OTD)) "
									+ "and (CI_ACCT_CHAR.CHAR_VAL LIKE :OTDELEN)  and (CI_ACCT_CHAR.EFFDT = (select MAX(EFFDT) from CI_ACCT_CHAR where CHAR_TYPE_CD in (:OTD))) \n");
												
						sb.append("WHERE \n");
						sb.append("SA.SA_TYPE_CD in (:SATP) and SA.SA_STATUS_FLG in (:SAAC) \n");

						sb.append(") FT1 \n");
						sb.append("join CI_ADJ_CL_CHAR NLG on NLG.ADJ_ID = FT1.SIBLING_ID and NLG.CHAR_TYPE_CD in (:NLG) \n");
						sb.append("join CI_ADJ_CALC_LN CHRG on CHRG.ADJ_ID = NLG.ADJ_ID and CHRG.SEQNO = NLG.SEQNO \n");
						sb.append("join CI_ADJ_CL_CHAR TRF on TRF.ADJ_ID = NLG.ADJ_ID and TRF.SEQNO = NLG.SEQNO and TRF.CHAR_TYPE_CD in (:TRF) \n");
						sb.append("join CI_ADJ_CL_CHAR NDSCH on NDSCH.ADJ_ID = NLG.ADJ_ID and NDSCH.CHAR_TYPE_CD in (:ISTR) "
								+ "and  TRIM(NDSCH.SEQNO) = TRIM(SUBSTR(NLG.ADHOC_CHAR_VAL, INSTR(NLG.ADHOC_CHAR_VAL, ',')+1)) --(Значение после «,») \n");

						sb.append("join CI_ADJ_CALC_LN NDS on NDS.ADJ_ID = NLG.ADJ_ID and NDS.SEQNO = NDSCH.SEQNO \n");
						sb.append("join CI_ADJ_CL_CHAR NDSTRF on NDSTRF.ADJ_ID = NLG.ADJ_ID and NDSTRF.CHAR_TYPE_CD in (:PNDS) and NDSTRF.SEQNO = NDSCH.SEQNO \n");
						sb.append("GROUP BY FT1.ACCT_ID, TRF.ADHOC_CHAR_VAL, NDSTRF.ADHOC_CHAR_VAL \n");

					}
					String queryRebill = sb.toString();


					queryRebill = queryRebill.replaceAll(":CRTP", CRTP);
					queryRebill = queryRebill.replaceAll(":SATP", SATP);
					queryRebill = queryRebill.replaceAll(":SAAC", SAAC);
					queryRebill = queryRebill.replaceAll(":NLG", NLG);
					queryRebill = queryRebill.replaceAll(":TRF", TRF);
					queryRebill = queryRebill.replaceAll(":ISTR", ISTR);
					queryRebill = queryRebill.replaceAll(":PNDS", PNDS);
					queryRebill = queryRebill.replaceAll(":OTDELEN", "'" + otdelen + "%'");
					queryRebill = queryRebill.replaceAll(":OTD", OTD);					

					PreparedStatement qRebill = null;
					Rebill va_Rebill = new Rebill();

					try {
						qRebill = createPreparedStatement(queryRebill);
						if (!test)
							qRebill.bindDate("v_EndDt", v_EndDt);
						if (!test)
							qRebill.bindDate("v_StartDt", v_StartDt);

						if (showLog)
							logger.info("qRebill = "+qRebill.toString());
						
						List<SQLResultRow> rebillList = qRebill.list();

						for (SQLResultRow rowRebill : rebillList) {
							String acctId = rowRebill.getString("ACCT_ID");
							BigDecimal Qty = rowRebill.getBigDecimal("QTY");
							BigDecimal TaxFreePrice = rowRebill.getBigDecimal("TAXFREEPRICE");
							BigDecimal Tax = rowRebill.getBigDecimal("TAX");
							BigDecimal TaxFreeCharge = rowRebill.getBigDecimal("TAXFREECHARGE");
							BigDecimal TaxSum = rowRebill.getBigDecimal("TAXSUM");
							BigDecimal Charge = rowRebill.getBigDecimal("CHARGE");

							TariffRebill Tr = new TariffRebill(acctId, Qty,
									TaxFreePrice, Tax, TaxFreeCharge, TaxSum,
									Charge);

							va_Rebill.addTariff(Tr);
							
							Company comp = NN.findCompany(acctId);
							if (comp == null)
							{
								comp = new Company("", "", "", acctId, "","", "", "", "", "", "", "", "", "", "", "", null, null, null);
								NN.addCompany(comp);								
							}
							//comp.addCorrections(tr);

						}
					} finally {
						if (qRebill != null)
							qRebill.close();
					}

					// Конец Корректировки 1

					// Корректировки 2

					sb.setLength(0);

					if (test) {
						sb.append("SELECT 1 as ACCT_ID, \n");
						sb.append("/*SUM( \n");
						sb.append("CASE FT1.FT_TYPE_FLG \n");
						sb.append("WHEN 'AD' THEN CHRG.BILL_SQ --(Для ФТ с типом «Корректировка» значение объема газа берется со знаком «+») \n");
						sb.append("WHEN 'AX' THEN -CHRG.BILL_SQ --(Для ФТ с типом «Отмена корректировки» значение объема газа берется со знаком «-») \n");
						sb.append("END)*/44*ROWNUM AS Qty, \n");
						sb.append("/*SUM(CHRG.CALC_AMT)*/ 55*ROWNUM as TaxFreeCharge, \n");
						sb.append("/*TRF.ADHOC_CHAR_VAL*/10*ROWNUM as TaxFreePrice, \n");
						sb.append("/*SUM(NDS.CALC_AMT) */ 55*ROWNUM as TaxSum, \n");
						sb.append("/*NDSTRF.ADHOC_CHAR_VAL*100*/22*ROWNUM as Tax, \n");
						sb.append("6667*ROWNUM as Charge \n");
						sb.append("from CI_CHAR_TYPE where ROWNUM<=5 \n");
					} else {

						sb.append("SELECT BILL.ACCT_ID, \n");
						sb.append("SUM( \n");
						sb.append("CASE CB.FT_TYPE_FLG \n");
						sb.append("WHEN 'BS' THEN CHRG.BILL_SQ --(Для ФТ с типом «Сегмент счета» значение объема газа берется со знаком «+») \n");
						sb.append("WHEN 'BX' THEN -CHRG.BILL_SQ --(Для ФТ с типом «Отмена сегмента счета» значение объема газа берется со знаком «-») \n");
						sb.append("END) AS Qty, \n");
						sb.append("SUM(CHRG.CALC_AMT) as TaxFreeCharge /* СуммаБезНДС */, \n");
						
						sb.append("case \n"
								+ "when trim(TRF.ADHOC_CHAR_VAL) is null or length(trim(TRF.ADHOC_CHAR_VAL))=0 then 0 \n"
								+ "else to_number(trim(replace(TRF.ADHOC_CHAR_VAL,',','.')),'99999999.999999999' )\n"
								+ "end as TaxFreePrice /* ЦенаБезНДС */, \n");
						
						sb.append("SUM(NDS.CALC_AMT) as TaxSum /* СуммаНДС */ , \n");
																
						sb.append("case \n"
								+ "when trim(NDSTRF.ADHOC_CHAR_VAL) is null or length(trim(NDSTRF.ADHOC_CHAR_VAL))=0 then 0 \n"
								+ "else to_number(trim(replace(NDSTRF.ADHOC_CHAR_VAL,',','.')),'99999999.999999999' ) \n"
								+ "end*100 as Tax /* ПроцентНДС */, \n");


						sb.append("(SUM(CHRG.CALC_AMT) + SUM(NDS.CALC_AMT)) as Charge /* ОбщаяСумма */ \n");

						sb.append("from ( \n");
						sb.append("SELECT DISTINCT BSREB.BSEG_ID, FTBX.FT_TYPE_FLG from CI_BILL  \n");
						sb.append("join CI_BSEG BSREB on BSREB.BILL_ID = CI_BILL.BILL_ID and BSREB.BSEG_STAT_FLG in (:BSST) and BSREB.END_DT < :v_StartDt \n");
						sb.append("join CI_FT FTBX on FTBX.SIBLING_ID = BSREB.BSEG_ID and FTBX.FT_TYPE_FLG in ('BS', 'BX') and FTBX.ACCOUNTING_DT between :v_StartDt and :v_EndDt \n");
						
						if (!isBlankOrNull(otdelen))
							sb.append("join CI_ACCT_CHAR on (CI_ACCT_CHAR.ACCT_ID = CI_BILL.ACCT_ID) and (CI_ACCT_CHAR.CHAR_TYPE_CD in (:OTD)) "
									+ "and (CI_ACCT_CHAR.CHAR_VAL LIKE :OTDELEN)  and (CI_ACCT_CHAR.EFFDT = (select MAX(EFFDT) from CI_ACCT_CHAR where CHAR_TYPE_CD in (:OTD))) \n");
						sb.append("where CI_BILL.BILL_ID = BSREB.BILL_ID \n");
						sb.append(") CB \n");
						sb.append("join CI_BSEG_CL_CHAR NLG on NLG.BSEG_ID = CB.BSEG_ID and NLG.CHAR_TYPE_CD in (:NLG) \n");
						sb.append("join CI_BSEG_CALC_LN CHRG on CHRG.BSEG_ID = NLG.BSEG_ID and CHRG.HEADER_SEQ = NLG.HEADER_SEQ and CHRG.SEQNO = NLG.SEQNO \n");
						sb.append("join CI_BSEG_CL_CHAR TRF on NLG.BSEG_ID = TRF.BSEG_ID and TRF.CHAR_TYPE_CD in (:TRF) and NLG.HEADER_SEQ = TRF.HEADER_SEQ and NLG.SEQNO = TRF.SEQNO \n");

						sb.append("join CI_BSEG_CL_CHAR NDSCH on NDSCH.BSEG_ID = NLG.BSEG_ID and NDSCH.CHAR_TYPE_CD in (:ISTR) and "
								+ "TRIM(NDSCH.HEADER_SEQ) = TRIM(SUBSTR(NLG.ADHOC_CHAR_VAL, 1, INSTR(NLG.ADHOC_CHAR_VAL, ',')-1)) --(Значение до «,») и  \n");
						sb.append("and TRIM(NDSCH.SEQNO) = TRIM(SUBSTR(NLG.ADHOC_CHAR_VAL, INSTR(NLG.ADHOC_CHAR_VAL, ',')+1)) -- (Значение после «,») \n");
						sb.append("join CI_BSEG_CALC_LN NDS on NDS.BSEG_ID = NLG.BSEG_ID and NDS.HEADER_SEQ = NDSCH.HEADER_SEQ and NDS.SEQNO = NDSCH.SEQNO \n");
						sb.append("join CI_BSEG_CL_CHAR NDSTRF on NDSTRF.BSEG_ID = NLG.BSEG_ID and NDSTRF.CHAR_TYPE_CD in (:PNDS) and NDSTRF.HEADER_SEQ = NDSCH.HEADER_SEQ and NDSTRF.SEQNO = NDSCH.SEQNO \n");
						sb.append("join CI_BSEG BS on BS.BSEG_ID = NLG.BSEG_ID \n");
						sb.append("join CI_BILL BILL on BILL.BILL_ID = BS.BILL_ID \n");
						
						sb.append("GROUP BY TRF.ADHOC_CHAR_VAL, NDSTRF.ADHOC_CHAR_VAL, BILL.ACCT_ID \n");

					}

					String queryRebill2 = sb.toString();

					queryRebill2 = queryRebill2.replaceAll(":BSST", SATP);
					queryRebill2 = queryRebill2.replaceAll(":NLG", NLG);
					queryRebill2 = queryRebill2.replaceAll(":TRF", TRF);
					queryRebill2 = queryRebill2.replaceAll(":ISTR", ISTR);
					queryRebill2 = queryRebill2.replaceAll(":PNDS", PNDS);
					queryRebill2 = queryRebill2.replaceAll(":OTDELEN", "'" + otdelen + "%'");
					queryRebill2 = queryRebill2.replaceAll(":OTD", OTD);							

					PreparedStatement qRebill2 = null;
					try {
						qRebill2 = createPreparedStatement(queryRebill2);
						if (!test)
							qRebill2.bindDate("v_EndDt", v_EndDt);
						if (!test)
							qRebill2.bindDate("v_StartDt", v_StartDt);

						if (showLog)
							logger.info("qRebill2 = "+qRebill2.toString());
						
						List<SQLResultRow> rebillList2 = qRebill2.list();

						// Rebill va_Rebill = new Rebill();

						for (SQLResultRow rowRebill2 : rebillList2) {
							String acctId = rowRebill2.getString("ACCT_ID");
							BigDecimal Qty = rowRebill2.getBigDecimal("QTY");
							BigDecimal TaxFreePrice = rowRebill2.getBigDecimal("TAXFREEPRICE");
							BigDecimal Tax = rowRebill2.getBigDecimal("TAX");
							BigDecimal TaxFreeCharge = rowRebill2.getBigDecimal("TAXFREECHARGE");
							BigDecimal TaxSum = rowRebill2.getBigDecimal("TAXSUM");
							BigDecimal Charge = rowRebill2.getBigDecimal("CHARGE");

							TariffRebill Tr = new TariffRebill(acctId, Qty,
									TaxFreePrice, Tax, TaxFreeCharge, TaxSum,
									Charge);
							va_Rebill.addTariff(Tr);
							
							Company comp = NN.findCompany(acctId);
							if (comp == null)
							{
								comp = new Company("", "", "", acctId, "","", "", "", "", "", "", "", "", "", "", "", null, null, null);
								NN.addCompany(comp);								
							}							

						}
					} finally {
						if (qRebill2 != null)
							qRebill2.close();
					}

					
					Tariffs tt = new Tariffs();

					for (TariffRebill rowRebill : va_Rebill.getTariffs()) {
						String acctId = rowRebill.getAcctId();
						
						BigDecimal Qty = rowRebill.getQty();
						// Double TaxFreePrice =
						// Double.valueOf(rowRebill.getTaxFreePrice()); //!!
						// Double Tax = Double.valueOf(rowRebill.getTax()); //!!
						BigDecimal TaxFreeCharge = rowRebill.getTaxFreeCharge();
						BigDecimal TaxSum = rowRebill.getTaxSum();
						BigDecimal Charge = rowRebill.getCharge();

						Tariff t = tt.searchTariff(acctId, rowRebill.getTax(),
								rowRebill.getTaxFreePrice());
						if (t == null) {
							t = new Tariff(acctId, rowRebill.getQty(),
									rowRebill.getTaxFreePrice(),
									rowRebill.getTax(),
									rowRebill.getTaxFreeCharge(),
									rowRebill.getTaxSum(),
									rowRebill.getCharge());
							tt.addTariff(t);
						}

						else {
							t.setObiem(t.getObiem().add(Qty));
							t.setSummaBezNDS(t.getSummaBezNDS().add(TaxFreeCharge));
							t.setSummaNDS(t.getSummaNDS().add(TaxSum));
							t.setObshhajaSumma(t.getObshhajaSumma().add(Charge));
						}

					}


					// Конец Корректировки 2
					

					sb.setLength(0);
					if (test) {

						sb.append("select * from (SELECT  ROWNUM as RR, ");
						sb.append("/* ACCT.ACCT_ID*/ROWNUM LSPredprijatija, \n");
						sb.append("/*NAME.ENTITY_NAME*/'222' Naimenovanie, \n");
						sb.append("1 KodPredprijatija, \n");
						sb.append("'aa' as POSTAL, \n");
						sb.append("'bb' as COUNTY, \n");
						sb.append("'cc' as CITY, \n");
						sb.append("'dd' as ADDRESS3, \n");
						sb.append("'ee' as GEO_CODE, \n");
						sb.append("'ff' as NUM2, \n");
						sb.append("/*BACCT.DFI_ID_NUM*/'4444' BankSvift, \n");
						sb.append("/*BACCTL.DESCR*/'5555' Bank, \n");
						sb.append("/*ACBNK.ADHOC_CHAR_VAL*/'6666' RaschSchet, \n");
						sb.append("/*DOG.ADHOC_CHAR_VAL*/'7777' NomerDogovora, \n");
						sb.append("/*DOGDT.ADHOC_CHAR_VAL*/TO_CHAR(TO_DATE('23-09-2015', 'DD-MM-YYYY'), 'YYYY.MM.DD') DataDogovora, \n");
						sb.append("/*FISK.PER_ID_NBR*/'9999' FiskKod, \n");
						sb.append("/*NDSCOD.PER_ID_NBR*/'00000' NDSKod, \n");
						sb.append("/*DELNAME.ENTITY_NAME*/'10101' ImjaDelegata, \n");
						sb.append("/*DOVNBR.ADHOC_CHAR_VAL*/'201010' NomerDoverennosti, \n");
						sb.append("/*DOVDT.ADHOC_CHAR_VAL*/TO_CHAR(TO_DATE('30-01-2016', 'DD-MM-YYYY'), 'YYYY.MM.DD') DataDoverennosti from CI_MSG_L) TT  where RR = :ACCT_ID  \n");
					} else {
						sb.append("SELECT ");
						sb.append("ACCT.ACCT_ID LSPredprijatija, \n");
						sb.append("NAME.ENTITY_NAME Naimenovanie, \n");
						sb.append("ACCT.ACCT_ID KodPredprijatija, \n");
						sb.append("PER.POSTAL, \n");
						sb.append("PER.COUNTY, \n");
						sb.append("PER.CITY, \n");
						sb.append("PER.ADDRESS3, \n");
						sb.append("PER.GEO_CODE, \n");
						sb.append("PER.NUM2, \n");
						
						sb.append("APSRC.DESCR BankSvift, \n");
						sb.append("APSRC.APAY_SRC_NAME Bank, \n");
						sb.append("AP.EXT_ACCT_ID  RaschSchet, \n");
						sb.append("DOG.ADHOC_CHAR_VAL NomerDogovora, \n");
						sb.append("TO_CHAR(TO_DATE(DOGDT.ADHOC_CHAR_VAL, 'DD-MM-YYYY'), 'YYYY.MM.DD') DataDogovora, \n");
						sb.append("FISK.PER_ID_NBR FiskKod, \n");
						sb.append("NDSCOD.PER_ID_NBR NDSKod, \n");
						sb.append("DELNAME.ENTITY_NAME ImjaDelegata, \n");
						sb.append("DOVNBR.ADHOC_CHAR_VAL NomerDoverennosti, \n");
						sb.append("TO_CHAR(TO_DATE(DOVDT.ADHOC_CHAR_VAL, 'DD-MM-YYYY'), 'YYYY.MM.DD') DataDoverennosti \n");
						sb.append("from CI_ACCT ACCT \n");
						sb.append("join CI_ACCT_PER ACPER on ACCT.ACCT_ID = ACPER.ACCT_ID and ACPER.MAIN_CUST_SW = 'Y' \n");
						sb.append("join CI_PER_NAME NAME on NAME.PER_ID = ACPER.PER_ID and	NAME.NAME_TYPE_FLG = 'PRIM' \n");
						sb.append("left join CI_PER PER on PER.PER_ID = ACPER.PER_ID  -- (Если не найдено, то NULL) \n");
						sb.append("left join CI_ACCT_APAY AP on AP.ACCT_ID = ACCT.ACCT_ID and AP.START_DT <=:v_EndDt and AP.END_DT >=:v_EndDt \n");
						sb.append("left join CI_APAY_SRC_L APSRC on APSRC.APAY_SRC_CD = AP.APAY_SRC_CD and APSRC.LANGUAGE_CD = :LANG -- (Если не найдено, то NULL) \n");
						sb.append("join CI_ACCT_CHAR DOG on DOG.ACCT_ID = ACCT.ACCT_ID and DOG.CHAR_TYPE_CD in (:DGNB) and DOG.EFFDT<=:v_EndDt \n");
						sb.append("left join CI_ACCT_CHAR DOGDT on DOGDT.ACCT_ID = ACCT.ACCT_ID and DOGDT.CHAR_TYPE_CD in (:DGDT) and DOGDT.EFFDT<=:v_EndDt  -- (Если не найдено, то NULL) \n");
						sb.append("left join CI_PER_ID FISK on FISK.PER_ID = ACPER.PER_ID and FISK.ID_TYPE_CD in (:FISK) -- (Если не найдено, то NULL) \n");
						sb.append("left join CI_PER_ID NDSCOD on NDSCOD.PER_ID = ACPER.PER_ID and NDSCOD.ID_TYPE_CD in (:NDSC) -- (Если не найдено, то NULL) \n");
						sb.append("left join CI_ACCT_PER DEL on DEL.ACCT_ID = ACCT.ACCT_ID and DEL.ACCT_REL_TYPE_CD in (:OTV) --  (Если не найдено, то NULL) \n");
						sb.append("left join CI_PER_NAME DELNAME on DELNAME.PER_ID = DEL.PER_ID and DELNAME.NAME_TYPE_FLG = 'PRIM' -- (Если не найдено, то NULL) \n");
						sb.append("left join CI_PER_CHAR DOVNBR on DOVNBR.PER_ID = DEL.PER_ID and DOVNBR.CHAR_TYPE_CD  in (:DOVN) and DOVNBR.EFFDT<=:v_EndDt -- (Если не найдено, то NULL) \n");
						sb.append("left join CI_PER_CHAR DOVDT on DOVDT.PER_ID = DEL.PER_ID and DOVDT.CHAR_TYPE_CD in (:DOVD) and DOVDT.EFFDT<=:v_EndDt  -- (Если не найдено, то NULL) \n");

						sb.append("WHERE \n");
						sb.append("ACCT.CUST_CL_CD in (:CUST) and ACCT.ACCT_ID = :ACCT_ID \n");
					
					}
					query = sb.toString();

					
					query = query.replaceAll(":BNKL", BNKL);
					query = query.replaceAll(":LANG", "'" + lang + "'");
					query = query.replaceAll(":OTDELEN", "'" + otdelen + "%'");
					query = query.replaceAll(":DGNB", DGNB);
					query = query.replaceAll(":DGDT", DGDT);

					query = query.replaceAll(":FISK", FISK);
					query = query.replaceAll(":NDSC", NDSC);

					query = query.replaceAll(":OTV", OTV);
					query = query.replaceAll(":DOVN", DOVN);
					query = query.replaceAll(":DOVD", DOVD);
					query = query.replaceAll(":OTD", OTD);
					query = query.replaceAll(":CUST", CUST);
					PreparedStatement q = null;
					q = createPreparedStatement(query);
					
					q.setAutoclose(false);
					
					try {
						
					//for (Company comp : NN.getCompanies()) {
					int indexCompany = 0;
					while (indexCompany<NN.getCompanies().size())
					{
					
						Company comp = NN.getCompanies().get(indexCompany);
						
						q.bindId("ACCT_ID",  new Account_Id(comp.getLSPredprijatija()));
						
						if (!test)
						{
						
							q.bindDate("v_EndDt", v_EndDt);
						}
						
						logger.info("size = [" + q.list().size() + "]");

						if (showLog)
							logger.info("q = "+q.toString());

						List<SQLResultRow> accList = q.list();
										
						
						for (SQLResultRow row : accList) {
							// row.toString()
							String SerijaNakladnoj = "";
							String NomerNakladnoj = "";
							String DataNakladnoj = toYYYYMMDD(v_EndDt);
							String LSPredprijatija = row.getString("LSPREDPRIJATIJA");
							String KodPredprijatija = row.getString("KODPREDPRIJATIJA");;
							String Naimenovanie = row.getString("NAIMENOVANIE");
							
							String pochtOtdel = row.getString("POSTAL");
							String adm = row.getString("COUNTY");
							String gorod = row.getString("CITY");
							String ulica = row.getString("ADDRESS3");
							String dom = row.getString("GEO_CODE");
							String kvartira = row.getString("NUM2");
							
							Address address = new Address(pochtOtdel, adm, gorod, ulica, dom, kvartira);
							
							if (isBlankOrNull(pochtOtdel) && isBlankOrNull(adm) && isBlankOrNull(gorod) && isBlankOrNull(ulica) && isBlankOrNull(dom) && isBlankOrNull(kvartira))
								addWarning((Md_072_01_MessageRepository.noAccountAddress(LSPredprijatija)));
							
							String Bank = row.getString("BANK");
							String BankSvift = row.getString("BANKSVIFT");
							String NomerDogovora = row.getString("NOMERDOGOVORA");
							
							String DataDogovora = row.getString("DATADOGOVORA");
							
							String RaschSchet = row.getString("RASCHSCHET");
							String FiskKod = row.getString("FISKKOD");
							String NDSKod = row.getString("NDSKOD");
							
							String NomerDoverennosti = (row.getString("NOMERDOVERENNOSTI")!=null) ?  row.getString("NOMERDOVERENNOSTI") :  "";
							
							String DataDoverennosti = (row.getString("DATADOVERENNOSTI")!=null) ?  row.getString("DATADOVERENNOSTI") :  "";
									
							String ImjaDelegata = (row.getString("IMJADELEGATA")!=null) ?  row.getString("IMJADELEGATA") :  ""; 

							BigDecimal TaxFreeCharge = BigDecimal.ZERO;
							BigDecimal TaxSum = BigDecimal.ZERO;
							BigDecimal Charge = BigDecimal.ZERO;
							BigDecimal Obiem = BigDecimal.ZERO;

							
							//comp = NN.findCompany(LSPredprijatija);
							//if (comp != null )
							//{
							
								comp.setSerijaNakladnoj(SerijaNakladnoj);
								comp.setNomerNakladnoj(NomerNakladnoj);
								comp.setDataNakladnoj(DataNakladnoj);
								comp.setKodPredprijatija(KodPredprijatija);
								comp.setNaimenovanie(Naimenovanie);
								comp.setAddress(address);														
								
								comp.setBank(Bank);
								comp.setBankSvift(BankSvift);
								comp.setNomerDogovora(NomerDogovora);
								comp.setDataDogovora(DataDogovora);
								comp.setRaschSchet(RaschSchet);
								comp.setFiskKod(FiskKod);
								comp.setNDSKod(NDSKod);
								comp.setNomerDoverennosti(NomerDoverennosti);
								comp.setDataDoverennosti(DataDoverennosti);								
								comp.setImjaDelegata(ImjaDelegata);
								
								//String SummaBezNDS = "";
								//String SummaNDS = "";
								//String ObshhajaSumma = "";
								
								for (Tariff t : tt.getTariffs()) {
									if (t.getAcctId_T().equals(LSPredprijatija)) 
									{
										comp.addCorrections(t);
										
										Obiem = Obiem.add(t.getObiem());
										TaxFreeCharge = TaxFreeCharge.add(t.getSummaBezNDS());
										TaxSum = TaxSum.add(t.getSummaNDS());
										Charge = Charge.add(t.getObshhajaSumma());
										
									}
									
									
								}


								
								for (Tariff t : comp.getTariffs()) {
									if (t.getAcctId_T().equals(LSPredprijatija)) 
									{
										Obiem = Obiem.add(t.getObiem());
										TaxFreeCharge = TaxFreeCharge.add(t.getSummaBezNDS());
										TaxSum = TaxSum.add(t.getSummaNDS());
										Charge = Charge.add(t.getObshhajaSumma());
										
									}
									
									
								}
								
								comp.setSummaBezNDS(TaxFreeCharge);
								comp.setSummaNDS(TaxSum);
								comp.setObshhajaSumma(Charge);
						
								
								sb.setLength(0);
								if (test)
								{
									sb.append("SELECT 0 as PENI FROM CI_CHAR_TYPE where ROWNUM=1 \n"); 
								}
								else
								{
								sb.append("SELECT  NVL(SUM(FT.TOT_AMT),0.00) PENI FROM CI_FT FT \n"); 
								sb.append("JOIN CI_SA SA ON SA.SA_ID = FT.SA_ID AND SA.ACCT_ID = :ACCT_ID and SA.SA_TYPE_CD in (:PENI)\n");
								sb.append("WHERE FT.ACCOUNTING_DT BETWEEN :v_StartDt AND :v_EndDt AND	FT.FREEZE_SW = 'Y' \n"); 
								}
								
								String queryPeni = sb.toString();
								
								queryPeni = queryPeni.replaceAll(":ACCT_ID", LSPredprijatija);
								//queryPeni = queryPeni.replaceAll(":SACH", SACH);
								queryPeni = queryPeni.replaceAll(":PENI", PENI);

								PreparedStatement qP = null;
								try {
			
									qP = createPreparedStatement(queryPeni);

									if (!test)
									{
										qP.bindDate("v_EndDt", v_EndDt);
									
										qP.bindDate("v_StartDt", v_StartDt);
									}

									if (showLog)
										logger.info("qP = "+qP.toString());
									
									BigDecimal peni = qP.firstRow().getBigDecimal("PENI");
									
									Peni p = new Peni(peni);
									
									comp.setPeni(p);

								}
								finally
								{
									if (qP!=null)
										qP.close();
										
								}
													

//				2.9.3.	Для каждого элемента va_BILL/НалоговыеНакладные/Предприятие получить выборку адресов:								
								sb.setLength(0);
								if (test)
								{
									sb.append("SELECT /*CI_BILL_ROUTING.ADDRESS1(2,3,4) AdresDostavki,*/ 'MD' POSTAL,"
											+ "'BALTI' COUNTY, 'BALTI MUN' CITY, 'ADDRESS3' ADDRESS3, 'GEO_CODE' GEO_CODE, 'NUM2' as NUM2 from CI_CHAR_TYPE \n"); 
									sb.append("WHERE ROWNUM<=3 \n"); 
								}
								else
								{
								sb.append("SELECT /*CI_BILL_ROUTING.ADDRESS1(2,3,4) AdresDostavki,*/ CI_BILL_ROUTING.POSTAL,"
										+ "CI_BILL_ROUTING.COUNTY, CI_BILL_ROUTING.CITY, CI_BILL_ROUTING.ADDRESS3, CI_BILL_ROUTING.GEO_CODE, CI_BILL_ROUTING.NUM2 from CI_BILL \n"); 
								sb.append("JOIN CI_BILL_ROUTING on CI_BILL_ROUTING.BILL_ID = CI_BILL.BILL_ID \n");
								sb.append("WHERE CI_BILL.ACCT_ID=:ACCT_ID and CI_BILL.BILL_DT BETWEEN :v_StartDt AND :v_EndDt \n"); 
								}
								
								String queryAdresDostavki = sb.toString();
								
								queryAdresDostavki = queryAdresDostavki.replaceAll(":ACCT_ID", LSPredprijatija);

								PreparedStatement qAdresDostavki = null;
								try {
			
									qAdresDostavki = createPreparedStatement(queryAdresDostavki);

									if (!test)
									{
										qAdresDostavki.bindDate("v_EndDt", v_EndDt);
									
										qAdresDostavki.bindDate("v_StartDt", v_StartDt);
									}

									if (showLog)
										logger.info("qAdresDostavki = "+qAdresDostavki.toString());
									
									List<SQLResultRow> adrList = qAdresDostavki.list();
									

									comp.setSerijaNakladnoj(SER);
									comp.setNomerNakladnoj(startNumber.toString());
									lastNakl  = SER.concat("/".concat(startNumber.toString()));
									countNakl = countNakl.add(BigInteger.ONE);
									
									if ((startNumber.compareTo(endNumber)==0) && (usedSER ==false))
									{
										usedSER = true;
										SER = SER1;
										startNumber = startNumber1;
										endNumber = endNumber1;
									}
									else
									{
										if ((startNumber.compareTo(endNumber)==0))
										{
											logInfo(Md_072_01_MessageRepository.exceedNumber());
										}
									
										startNumber = startNumber.add(BigInteger.ONE);
									}

									
									int cc = 0;
									for (SQLResultRow rowD : adrList) {
									cc++;
									String pochtOtdelD = rowD.getString("POSTAL");
									String admD = rowD.getString("COUNTY");
									String gorodD = rowD.getString("CITY");
									String ulicaD = rowD.getString("ADDRESS3");
									String domD = rowD.getString("GEO_CODE");
									String kvartiraD = rowD.getString("NUM2");
									
									Address addressD = new Address(pochtOtdelD, admD, gorodD, ulicaD, domD, kvartiraD);
									if ( cc>1 )
									{
										comp = new Company(comp);
										comp.getAdresDostavki().clear();
										comp.setAdresDostavki(addressD);
										
										indexCompany++;
										NN.getCompanies().add(indexCompany, comp);										

									}
									else comp.setAdresDostavki(addressD);
									
									
									}

								}
								finally
								{
									if (qAdresDostavki!=null)
										qAdresDostavki.close();
										
								}
								
							
							//}
								
						}

						indexCompany++;	
					
					}
					
					} finally {
						if (q != null)
							q.close();
					}



				XStream xstream = new XStream();
				xstream.registerConverter(new ibmBigDecimalConverter());
				
				xstream.alias("НалоговыеНакладные", TaxBills.class);
				xstream.alias("Предприятие", Company.class);
				xstream.alias("Тариф", Tariff.class);
				xstream.alias("ТочкаУчета", ServicePoint.class);
				xstream.alias("Адрес", Address.class);
				xstream.alias("Пеня", Peni.class);
				
				xstream.aliasField("СерияНакладной", Company.class,
						"SerijaNakladnoj");
				xstream.aliasField("НомерНакладной", Company.class,
						"NomerNakladnoj");
				xstream.aliasField("ДатаНакладной", Company.class,
						"DataNakladnoj");
				xstream.aliasField("ЛСПредприятия", Company.class,
						"LSPredprijatija");
				xstream.aliasField("КодПредприятия", Company.class,
						"KodPredprijatija");
				xstream.aliasField("Наименование", Company.class,
						"Naimenovanie");
				xstream.aliasField("ПочтОтдел", Address.class, "PochtOtdel");
				xstream.aliasField("Адм", Address.class, "Adm");
				xstream.aliasField("Город", Address.class, "Gorod");
				xstream.aliasField("Улица", Address.class, "Ulica");
				xstream.aliasField("Дом", Address.class, "Dom");
				xstream.aliasField("Квартира", Address.class, "Kvartira");
				
				xstream.aliasField("Сумма", Peni.class, "Summa");
				
				xstream.aliasField("Адрес", Company.class, "address");
				xstream.aliasField("Пеня", Company.class, "peni");
				
				xstream.aliasField("Банк", Company.class, "Bank");
				xstream.aliasField("БанкСвифт", Company.class, "BankSvift");
				xstream.aliasField("НомерДоговора", Company.class,
						"NomerDogovora");
				xstream.aliasField("ДатаДоговора", Company.class,
						"DataDogovora");
				xstream.aliasField("РасчСчет", Company.class, "RaschSchet");
				xstream.aliasField("ФискКод", Company.class, "FiskKod");
				xstream.aliasField("НДСКод", Company.class, "NDSKod");
				xstream.aliasField("НомерДоверенности", Company.class,
						"NomerDoverennosti");
				xstream.aliasField("ДатаДоверенности", Company.class,
						"DataDoverennosti");
				xstream.aliasField("ИмяДелегата", Company.class, "ImjaDelegata");
				xstream.aliasField("СуммаБезНДС", Company.class, "SummaBezNDS");
				xstream.aliasField("СуммаНДС", Company.class, "SummaNDS");
				xstream.aliasField("ОбщаяСумма", Company.class, "ObshhajaSumma");

				xstream.aliasField("Потребление", Company.class, "Potreblenie");
				xstream.aliasField("Тарифы", Company.class, "tariffs");
				xstream.aliasField("Перерасчеты", Company.class, "corrections");
				xstream.aliasField("АдресДоставки", Company.class, "adresDostavki");
				
				xstream.aliasField("Объем", Tariff.class, "Obiem_T");
				xstream.aliasField("ЦенаБезНДС", Tariff.class, "CenaBezNDS_T");
				xstream.aliasField("ПроцентНДС", Tariff.class, "ProcentNDS_T");
				xstream.aliasField("СуммаБезНДС", Tariff.class, "SummaBezNDS_T");
				xstream.aliasField("СуммаНДС", Tariff.class, "SummaNDS_T");
				xstream.aliasField("ОбщаяСумма", Tariff.class,
						"ObshhajaSumma_T");

				xstream.aliasField("ТочкаУчета", ServicePoint.class,
						"TochkaUcheta");
				xstream.aliasField("АдресТУ", ServicePoint.class, "AdresTU");
				xstream.aliasField("НомерПУ", ServicePoint.class, "NomerPU");
				xstream.aliasField("НачПоказ", ServicePoint.class, "NachPokaz");
				xstream.aliasField("КонПоказ", ServicePoint.class, "KonPokaz");
				xstream.aliasField("Объем", ServicePoint.class, "Obiem");
				xstream.aliasField("ЕдИзм", ServicePoint.class, "EdIzm");

				xstream.addImplicitCollection(TaxBills.class, "companies");
				xstream.omitField(Tariff.class, "AcctId_T");
				//xstream.aliasField("", Tariff.class, "EdIzm");
			
				
				// xstream.addImplicitCollection(Company.class, "tariffs");
				// xstream.addImplicitCollection(Company.class, "Potreblenie");

				System.out.println(xstream.toXML(NN));
				String xml = xstream.toXML(NN);
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(path + fileName);
					fos.write("<?xml version=\"1.0\"?>\n".getBytes("UTF-8")); // write
																				// XML
																				// header,
																				// as
																				// XStream
																				// doesn't
																				// do
																				// that
																				// for
																				// us
					byte[] bytes = xml.getBytes("UTF-8");
					fos.write(bytes);

					logInfo(Md_072_01_MessageRepository.fileWasCreated(path.concat("/".concat(fileName)), countNakl, lastNakl));

				} catch (Exception e) {
					e.printStackTrace(); // this obviously needs to be refined.
				} finally {
					if (fos != null) {
						try {
							fos.close();
						} catch (IOException e) {
							e.printStackTrace(); // this obviously needs to be
													// refined.
						}
					}
				}


			// addError(Md_072_01_MessageRepository.noFN("sdsd"));

			// CheckExistingParams("","","", null);
			return true;
		}

	}

	@SuppressWarnings("deprecation")
	public List<String> CheckExistingParams(String wfm, String param,
			String query_s, List<String> list_values_return) {
		List<String> list_values = null;
		List<String> list_errors = null;
		PreparedStatement q = null;
		list_errors = new ArrayList<String>();

		try {
			list_values = CmApiFeatureConfigurationWrapper.getOptions(wfm,
					param);
			list_values_return.clear();
			list_values_return.addAll(list_values);
			if (test) {
				if (param.equals("SATP")) {
					list_values.clear();
					list_values.add("ULG-P");
				}
				// if (param.equals("CUST")) {
				// list_values.clear();
				// list_values.add("UR-L");
				// }

			}

			if (list_values.size() == 0) {
				addError(Md_072_01_MessageRepository.noParamFN(param, wfm));
				/* if (nrErrors++==maxErrors) */return null;
			}

		} catch (ApplicationError e) {
			addError(Md_072_01_MessageRepository.noParamFN(param, wfm));
			/* if (nrErrors++==maxErrors) */return null;
		}


			
		if (!param.equals("PENI"))
		
		for (String value : list_values) {
			try {

				StringBuilder sb = new StringBuilder();		
				sb.append(query_s);
				String query = sb.toString();

				query = query.replaceAll(":" + param, "'" + value + "'");
				
				q = createPreparedStatement(query);
			//q.bindString(param, value, null);
			
				logger.info("query = [" + q.toString() + "]");



				//logger.info("size = [" + q.list().size() + "]");

				if (q.list().size() == 0) {
					list_errors.add(value);
				} else {
					SQLResultRow row = q.firstRow();
					logger.info(param + " = [" + row.getString("VALUE") + "]");

				}

			}
		 finally {
			if (q != null) {
				q.close();
			}
		
		 }	
		

		}

		return list_errors;
	}
	
	public static String toYYYYMMDD (Date D) 
	{
		int day = D.getDay();
		int month = D.getMonth();
		int year = D.getYear();
		
		return year+"."+(month<10 ? "0" : "") + month+"."+day; 
	}

}
