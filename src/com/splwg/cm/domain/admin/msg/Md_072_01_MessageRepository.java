package com.splwg.cm.domain.admin.msg;

import java.math.BigInteger;

import com.splwg.base.domain.common.characteristicType.CharacteristicType;
import com.splwg.base.domain.common.featureConfiguration.FeatureConfiguration;
import com.splwg.base.domain.common.featureConfiguration.FeatureConfigurationOption;
import com.splwg.base.domain.common.message.MessageParameters;
import com.splwg.ccb.domain.todo.toDoCase.entity.ToDoCase;
import com.splwg.cm.domain.common.CmApiFeatureConfigurationWrapper;
import com.splwg.shared.common.ServerMessage;

/**
 * @messageRepository
 */

public class Md_072_01_MessageRepository extends Md_072_01_Messages {

	private static Md_072_01_MessageRepository instance;

	private static Md_072_01_MessageRepository getInstance() {
		if (instance == null) {
			instance = new Md_072_01_MessageRepository();
		}
		return instance;
	}

	/**
	 * @message NO_FN
	 * @description ENG "Не найдена настройка функциональности %1" "Не найдена настройка функциональности %1"
	 * @description RUS "Не найдена настройка функциональности %1" "Не найдена настройка функциональности %1"
	 * @description RUM "Не найдена настройка функциональности %1" "Не найдена настройка функциональности %1"
	 */
	public static ServerMessage noFN(String fc) {
		MessageParameters p = new MessageParameters();
		p.addRawString(fc);
		return getInstance().getMessage(NO_FN, p);
	}

	/**
	 * @message NO_PARAM_FN
	 * @description ENG "Не найден параметр %1 настройки функциональности %2" "Не найден параметр %1 настройки функциональности %2"
	 * @description RUS "Не найден параметр %1 настройки функциональности %2" "Не найден параметр %1 настройки функциональности %2"
	 * @description RUM "Не найден параметр %1 настройки функциональности %2" "Не найден параметр %1 настройки функциональности %2"
	 */
	public static ServerMessage noParamFN(String fco, String fc) {
		MessageParameters p = new MessageParameters();
		p.addRawString(fco);
		p.addRawString(fc);
		return getInstance().getMessage(NO_PARAM_FN, p);
	}


	/**
	 * @message NO_PATH
	 * @description ENG "Путь %1 не существует" "Путь %1 не существует"
	 * @description RUS "Путь %1 не существует" "Путь %1 не существует"
	 * @description RUM "Путь %1 не существует" "Путь %1 не существует"
	 */
	public static ServerMessage noPath(String path) { 
		MessageParameters p = new MessageParameters();
		p.addRawString(path);
		return getInstance().getMessage(NO_PATH, p);
	}
	
	
	
	/**
	 * @message NO_SA
	 * @description ENG "Указанный тип РДО отсутствует: %1" "Указанный тип РДО отсутствует: %1"
	 * @description RUS "Указанный тип РДО отсутствует: %1" "Указанный тип РДО отсутствует: %1"
	 * @description RUM "Указанный тип РДО отсутствует: %1" "Указанный тип РДО отсутствует: %1"
	 */
	public static ServerMessage noSA(String sa) { 
		MessageParameters p = new MessageParameters();
		p.addRawString(sa);
		return getInstance().getMessage(NO_SA, p);
	}
	
	
	/**
	 * @message NO_FIELD_VALUE
	 * @description ENG "В системном справочнике для поля %1 не найдено значение %2" "В системном справочнике для поля %1 не найдено значение %2"
	 * @description RUS "В системном справочнике для поля %1 не найдено значение %2" "В системном справочнике для поля %1 не найдено значение %2"
	 * @description RUM "В системном справочнике для поля %1 не найдено значение %2" "В системном справочнике для поля %1 не найдено значение %2"
	 */
	public static ServerMessage noFieldValue(String field, String value) {
		MessageParameters p = new MessageParameters();
		p.addRawString(field);
		p.addRawString(value);
		return getInstance().getMessage(NO_FIELD_VALUE, p);
	}	
	

	/**
	 * @message ERROR_CREATING_FILE
	 * @description ENG "Невозможно создать файл %1%2" "Невозможно создать файл %1%2"
	 * @description RUS "Невозможно создать файл %1%2" "Невозможно создать файл %1%2"
	 * @description RUM "Невозможно создать файл %1%2" "Невозможно создать файл %1%2"
	 */
	public static ServerMessage errorCreatingFile(String paramValue, String fileName) {
		MessageParameters p = new MessageParameters();
		p.addRawString(paramValue);
		p.addRawString(fileName);
		return getInstance().getMessage(ERROR_CREATING_FILE, p);
	}		
	
	
	/**
	 * @message MAX_ERRORS_REACHED
	 * @description ENG "Количество ошибок достигло максимального значения: %1" "Количество ошибок достигло максимального значения: %1"
	 * @description RUS "Количество ошибок достигло максимального значения: %1" "Количество ошибок достигло максимального значения: %1"
	 * @description RUM "Количество ошибок достигло максимального значения: %1" "Количество ошибок достигло максимального значения: %1"
	 */
	public static ServerMessage reachedMaxErrors(BigInteger maxErrors) {
		MessageParameters p = new MessageParameters();
		p.addBigInteger(maxErrors);
		return getInstance().getMessage(MAX_ERRORS_REACHED, p);
	}
	
	
	/**
	 * @message FILE_WAS_CREATED
	 * @description ENG "Создан файл %1. Выгружено накладных %2. Серия/номер последнего документа: %3" "Создан файл %1. Выгружено накладных %2. Серия/номер последнего документа: %3"
	 * @description RUS "Создан файл %1. Выгружено накладных %2. Серия/номер последнего документа: %3" "Создан файл %1. Выгружено накладных %2. Серия/номер последнего документа: %3"
	 * @description RUM "Создан файл %1. Выгружено накладных %2. Серия/номер последнего документа: %3" "Создан файл %1. Выгружено накладных %2. Серия/номер последнего документа: %3"
	 */
	public static ServerMessage fileWasCreated(String fileName, BigInteger nr, String seriaNumber) {
		MessageParameters p = new MessageParameters();
		p.addRawString(fileName);
		p.addBigInteger(nr);
		p.addRawString(seriaNumber);
		return getInstance().getMessage(FILE_WAS_CREATED, p);
	}		
	
	
	/**
	 * @message NO_CUST_CATEGORY
	 * @description ENG "Указанная категория абонента отсутствует: %1" "Указанная категория абонента отсутствует: %1"
	 * @description RUS "Указанная категория абонента отсутствует: %1" "Указанная категория абонента отсутствует: %1"
	 * @description RUM "Указанная категория абонента отсутствует: %1" "Указанная категория абонента отсутствует: %1"
	 */
	public static ServerMessage noCustCategory(String category) {
		MessageParameters p = new MessageParameters();
		p.addRawString(category);
		return getInstance().getMessage(NO_CUST_CATEGORY, p);
	}
	
	
	
	/**
	 * @message NO_CHARTYPE_OBJECT
	 * @description ENG "Для объекта %3 тип характеристики не найден: %1, %2" "Для объекта %3 тип характеристики не найден: %1, %2"
	 * @description RUS "Для объекта %3 тип характеристики не найден: %1, %2" "Для объекта %3 тип характеристики не найден: %1, %2"
	 * @description RUM "Для объекта %3 тип характеристики не найден: %1, %2" "Для объекта %3 тип характеристики не найден: %1, %2"
	 */
	public static ServerMessage noChartypeForObject(String param, String paramValue, String object) {
		MessageParameters p = new MessageParameters();
		p.addRawString(param);
		p.addRawString(paramValue);
		p.addRawString(object);
		return getInstance().getMessage(NO_CHARTYPE_OBJECT, p);
	}	
	
	
	
	/**
	 * @message NO_TYPE_RELATION
	 * @description ENG "Тип отношения %1 не найден %2" "Тип отношения %1 не найден %2"
	 * @description RUS "Тип отношения %1 не найден %2" "Тип отношения %1 не найден %2"
	 * @description RUM "Тип отношения %1 не найден %2" "Тип отношения %1 не найден %2"
	 */
	public static ServerMessage noTypeRelation(String param, String typeRelation) {
		MessageParameters p = new MessageParameters();
		p.addRawString(param);
		p.addRawString(typeRelation);
		return getInstance().getMessage(NO_TYPE_RELATION, p);
	}
	
	
	/**
	 * @message NO_TYPE_IDENTITY
	 * @description ENG "Тип идентификатора не найден: %1 %2" "Тип идентификатора не найден: %1 %2"
	 * @description RUS "Тип идентификатора не найден: %1 %2" "Тип идентификатора не найден: %1 %2"
	 * @description RUM "Тип идентификатора не найден: %1 %2" "Тип идентификатора не найден: %1 %2"
	 */
	public static ServerMessage noTypeIdenitity(String param, String paramValue) {
		MessageParameters p = new MessageParameters();
		p.addRawString(param);
		p.addRawString(paramValue);
		return getInstance().getMessage(NO_TYPE_IDENTITY, p);
	}		
		
	/**
	 * @message NO_TYPE_CORR
	 * @description ENG "Не найден тип корректировки %1" "Не найден тип корректировки %1"
	 * @description RUS "Не найден тип корректировки %1" "Не найден тип корректировки %1"
	 * @description RUM "Не найден тип корректировки %1" "Не найден тип корректировки %1"
	 */
	public static ServerMessage noTypeCorr(String param) {
		MessageParameters p = new MessageParameters();
		p.addRawString(param);
		return getInstance().getMessage(NO_TYPE_CORR, p);
	}	
	
	/**
	 * @message NO_TYPE_CHAR_FOR_CHAR_TYPE
	 * @description ENG "Значение характеристики %1 для типа характеристики %2 не найден" "Значение характеристики %1 для типа характеристики %2 не найден"
	 * @description RUS "Значение характеристики %1 для типа характеристики %2 не найден" "Значение характеристики %1 для типа характеристики %2 не найден"
	 * @description RUM "Значение характеристики %1 для типа характеристики %2 не найден" "Значение характеристики %1 для типа характеристики %2 не найден"
	 */
	public static ServerMessage noTypeCharForCharType(String charValue, String charType) {
		MessageParameters p = new MessageParameters();
		p.addRawString(charValue);
		p.addRawString(charType);
		return getInstance().getMessage(NO_TYPE_CHAR_FOR_CHAR_TYPE, p);
	}	

	
	/**
	 * @message NO_ACCOUNT_ADDRESS
	 * @description ENG "Не найден юридический адрес для Л/С: %1" "Не найден юридический адрес для Л/С: %1"
	 * @description RUS "Не найден юридический адрес для Л/С: %1" "Не найден юридический адрес для Л/С: %1"
	 * @description RUM "Не найден юридический адрес для Л/С: %1" "Не найден юридический адрес для Л/С: %1"
	 */
	public static ServerMessage noAccountAddress(String account) {
		MessageParameters p = new MessageParameters();
		p.addRawString(account);
		return getInstance().getMessage(NO_ACCOUNT_ADDRESS, p);
	}	


	/**
	 * @message EXCEED_NUMBER
	 * @description ENG "Превышен указанный в параметрах диапазон номеров накладных" "Превышен указанный в параметрах диапазон номеров накладных"
	 * @description RUS "Превышен указанный в параметрах диапазон номеров накладных" "Превышен указанный в параметрах диапазон номеров накладных"
	 * @description RUM "Превышен указанный в параметрах диапазон номеров накладных" "Превышен указанный в параметрах диапазон номеров накладных"
	 */
	public static ServerMessage exceedNumber() {
		MessageParameters p = new MessageParameters();
		return getInstance().getMessage(EXCEED_NUMBER, p);
	}

	/**
	 * @message FIELDS_NOT_FILLED
	 * @description ENG "Не заполнены поля %1" "Не заполнены поля %1"
	 * @description RUS "Не заполнены поля %1" "Не заполнены поля %1"
	 * @description RUM "Не заполнены поля %1" "Не заполнены поля %1"
	 */
	public static ServerMessage fieldsNotFilled(String fields) {
		MessageParameters p = new MessageParameters();
		p.addRawString(fields);
		return getInstance().getMessage(FIELDS_NOT_FILLED, p);
	}	
	 	
	
}
