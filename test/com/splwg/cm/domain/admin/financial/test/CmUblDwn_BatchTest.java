package com.splwg.cm.domain.admin.financial.test;

import com.splwg.base.api.batch.SubmissionParameters;
import com.splwg.base.api.datatypes.Date;
import com.splwg.base.domain.batch.batchControl.BatchControl;
import com.splwg.base.domain.batch.batchRun.BatchRun;
import com.splwg.base.domain.security.user.User_Id;
import com.splwg.base.support.sql.FunctionReplacerHelper;

import ru.ibs.test.framework.algorithm.AlienBatchTestCase;

import com.splwg.base.api.lookup.CustomizationOwnerLookup;

import com.splwg.cm.domain.financial.CmUblDwn_Batch;
public class CmUblDwn_BatchTest extends AlienBatchTestCase 
{

	@Override
	protected SubmissionParameters setupRun(SubmissionParameters paramSubmissionParameters) {
		FunctionReplacerHelper.enableNoDBSpecificFunctionValidation(false);
        BatchControl batchControl = createNewBatchControl(CmUblDwn_Batch.class, CustomizationOwnerLookup.constants.CM);
        
        paramSubmissionParameters.setBatchControlId(batchControl.getId());
        paramSubmissionParameters.setUserId(new User_Id("VMOGLAN"));
        paramSubmissionParameters.setProcessDate(new Date(2015, 1, 31));
        
        addSoftParameter(batchControl, paramSubmissionParameters, "WFM", "CMUBLDWN", true);
        //addSoftParameter(batchControl, paramSubmissionParameters, "OTDELEN", "01", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "LANG", "RUS", true);
        addSoftParameter(batchControl, paramSubmissionParameters, "TEST", "N", true);
        
        addSoftParameter(batchControl, paramSubmissionParameters, "SER", "MG", true);
        addSoftParameter(batchControl, paramSubmissionParameters, "START_NUMBER", "100", true);
        addSoftParameter(batchControl, paramSubmissionParameters, "END_NUMBER", "110", true);

        addSoftParameter(batchControl, paramSubmissionParameters, "SER1", "MG1", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "START_NUMBER1", "1001", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "END_NUMBER1", "1101", false);
        
        System.out.println(">>>>>>>>>>>>>>>START<<<<<<<<<<<<<<<<<");
        
        CmUblDwn_Batch.test = true;
        CmUblDwn_Batch.showLog = true;
        //ComponentContainer coponentContainer = ContextHolder.getContext().getContainer();
        return paramSubmissionParameters;
	}

	@Override
	protected void validateResults(BatchRun arg0) {
		System.out.println("validateResults is invoked");
		
	}

}
